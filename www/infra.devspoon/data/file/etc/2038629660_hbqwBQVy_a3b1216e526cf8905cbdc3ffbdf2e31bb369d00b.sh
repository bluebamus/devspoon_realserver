#!/bin/sh

git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
wget -P /usr/share/vim/vim74/colors http://hello-world.kr/wp-content/uploads/2017/02/yw.vim
echo ":colorscheme yw" > ~/.vimrc
vim +PluginInstall +qall
wget -P ~/.vim/syntax http://hello-world.kr/wp-content/uploads/2017/02/python.vim
wget -P ~/.vim/ http://hello-world.kr/wp-content/uploads/2017/02/pydiction-1.2.3.zip
unzip ~/.vim/pydiction-1.2.3.zip -d ~/.vim/
mv ~/.vim/pydiction/after ~/.vim/
rm -f ~/.vim/pydiction-1.2.3.zip
