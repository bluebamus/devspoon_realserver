#!/bin/sh

git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
wget -P /usr/share/vim/vim74/colors http://hello-world.kr/wp-content/uploads/2017/02/yw.vim
echo ":colorscheme yw" > ~/.vimrc
vim +PluginInstall +qall
