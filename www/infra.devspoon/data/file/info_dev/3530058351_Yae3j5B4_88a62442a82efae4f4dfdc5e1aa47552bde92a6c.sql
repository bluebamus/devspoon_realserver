-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- 생성 시간: 19-01-03 11:39
-- 서버 버전: 10.1.25-MariaDB-1~xenial
-- PHP 버전: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `mt_solution`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `authentication`
--

CREATE TABLE `authentication` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add blacklist users', 1, 'add_blacklistusers'),
(2, 'Can change blacklist users', 1, 'change_blacklistusers'),
(3, 'Can delete blacklist users', 1, 'delete_blacklistusers'),
(4, 'Can add auth group', 2, 'add_authgroup'),
(5, 'Can change auth group', 2, 'change_authgroup'),
(6, 'Can delete auth group', 2, 'delete_authgroup'),
(7, 'Can add reject users', 3, 'add_rejectusers'),
(8, 'Can change reject users', 3, 'change_rejectusers'),
(9, 'Can delete reject users', 3, 'delete_rejectusers'),
(10, 'Can add sending failed users', 4, 'add_sendingfailedusers'),
(11, 'Can change sending failed users', 4, 'change_sendingfailedusers'),
(12, 'Can delete sending failed users', 4, 'delete_sendingfailedusers'),
(13, 'Can add company info list', 5, 'add_companyinfolist'),
(14, 'Can change company info list', 5, 'change_companyinfolist'),
(15, 'Can delete company info list', 5, 'delete_companyinfolist'),
(16, 'Can add naver company mailing users', 6, 'add_navercompanymailingusers'),
(17, 'Can change naver company mailing users', 6, 'change_navercompanymailingusers'),
(18, 'Can delete naver company mailing users', 6, 'delete_navercompanymailingusers'),
(19, 'Can add django admin log', 7, 'add_djangoadminlog'),
(20, 'Can change django admin log', 7, 'change_djangoadminlog'),
(21, 'Can delete django admin log', 7, 'delete_djangoadminlog'),
(22, 'Can add django migrations', 8, 'add_djangomigrations'),
(23, 'Can change django migrations', 8, 'change_djangomigrations'),
(24, 'Can delete django migrations', 8, 'delete_djangomigrations'),
(25, 'Can add django content type', 9, 'add_djangocontenttype'),
(26, 'Can change django content type', 9, 'change_djangocontenttype'),
(27, 'Can delete django content type', 9, 'delete_djangocontenttype'),
(28, 'Can add managed exgroup users', 10, 'add_managedexgroupusers'),
(29, 'Can change managed exgroup users', 10, 'change_managedexgroupusers'),
(30, 'Can delete managed exgroup users', 10, 'delete_managedexgroupusers'),
(31, 'Can add auth permission', 11, 'add_authpermission'),
(32, 'Can change auth permission', 11, 'change_authpermission'),
(33, 'Can delete auth permission', 11, 'delete_authpermission'),
(34, 'Can add auth group permissions', 12, 'add_authgrouppermissions'),
(35, 'Can change auth group permissions', 12, 'change_authgrouppermissions'),
(36, 'Can delete auth group permissions', 12, 'delete_authgrouppermissions'),
(37, 'Can add auth user user permissions', 13, 'add_authuseruserpermissions'),
(38, 'Can change auth user user permissions', 13, 'change_authuseruserpermissions'),
(39, 'Can delete auth user user permissions', 13, 'delete_authuseruserpermissions'),
(40, 'Can add job status', 14, 'add_jobstatus'),
(41, 'Can change job status', 14, 'change_jobstatus'),
(42, 'Can delete job status', 14, 'delete_jobstatus'),
(43, 'Can add auth user', 15, 'add_authuser'),
(44, 'Can change auth user', 15, 'change_authuser'),
(45, 'Can delete auth user', 15, 'delete_authuser'),
(46, 'Can add naver keyword job order', 16, 'add_naverkeywordjoborder'),
(47, 'Can change naver keyword job order', 16, 'change_naverkeywordjoborder'),
(48, 'Can delete naver keyword job order', 16, 'delete_naverkeywordjoborder'),
(49, 'Can add django session', 17, 'add_djangosession'),
(50, 'Can change django session', 17, 'change_djangosession'),
(51, 'Can delete django session', 17, 'delete_djangosession'),
(52, 'Can add authentication', 18, 'add_authentication'),
(53, 'Can change authentication', 18, 'change_authentication'),
(54, 'Can delete authentication', 18, 'delete_authentication'),
(55, 'Can add naver compnay exgroup users', 19, 'add_navercompnayexgroupusers'),
(56, 'Can change naver compnay exgroup users', 19, 'change_navercompnayexgroupusers'),
(57, 'Can delete naver compnay exgroup users', 19, 'delete_navercompnayexgroupusers'),
(58, 'Can add contract', 20, 'add_contract'),
(59, 'Can change contract', 20, 'change_contract'),
(60, 'Can delete contract', 20, 'delete_contract'),
(61, 'Can add keyword list', 21, 'add_keywordlist'),
(62, 'Can change keyword list', 21, 'change_keywordlist'),
(63, 'Can delete keyword list', 21, 'delete_keywordlist'),
(64, 'Can add naver gathered users', 22, 'add_navergatheredusers'),
(65, 'Can change naver gathered users', 22, 'change_navergatheredusers'),
(66, 'Can delete naver gathered users', 22, 'delete_navergatheredusers'),
(67, 'Can add auth user groups', 23, 'add_authusergroups'),
(68, 'Can change auth user groups', 23, 'change_authusergroups'),
(69, 'Can delete auth user groups', 23, 'delete_authusergroups'),
(70, 'Can add log entry', 24, 'add_logentry'),
(71, 'Can change log entry', 24, 'change_logentry'),
(72, 'Can delete log entry', 24, 'delete_logentry'),
(73, 'Can add group', 25, 'add_group'),
(74, 'Can change group', 25, 'change_group'),
(75, 'Can delete group', 25, 'delete_group'),
(76, 'Can add permission', 26, 'add_permission'),
(77, 'Can change permission', 26, 'change_permission'),
(78, 'Can delete permission', 26, 'delete_permission'),
(79, 'Can add user', 27, 'add_user'),
(80, 'Can change user', 27, 'change_user'),
(81, 'Can delete user', 27, 'delete_user'),
(82, 'Can add content type', 28, 'add_contenttype'),
(83, 'Can change content type', 28, 'change_contenttype'),
(84, 'Can delete content type', 28, 'delete_contenttype'),
(85, 'Can add session', 29, 'add_session'),
(86, 'Can change session', 29, 'change_session'),
(87, 'Can delete session', 29, 'delete_session'),
(88, 'Can add company list', 30, 'add_companylist'),
(89, 'Can change company list', 30, 'change_companylist'),
(90, 'Can delete company list', 30, 'delete_companylist'),
(91, 'Can add job list', 31, 'add_joblist'),
(92, 'Can change job list', 31, 'change_joblist'),
(93, 'Can delete job list', 31, 'delete_joblist'),
(94, 'Can add mb list', 32, 'add_mblist'),
(95, 'Can change mb list', 32, 'change_mblist'),
(96, 'Can delete mb list', 32, 'delete_mblist'),
(97, 'Can add payment', 33, 'add_payment'),
(98, 'Can change payment', 33, 'change_payment'),
(99, 'Can delete payment', 33, 'delete_payment'),
(100, 'Can add service list', 34, 'add_servicelist'),
(101, 'Can change service list', 34, 'change_servicelist'),
(102, 'Can delete service list', 34, 'delete_servicelist'),
(103, 'Can add sns list', 35, 'add_snslist'),
(104, 'Can change sns list', 35, 'change_snslist'),
(105, 'Can delete sns list', 35, 'delete_snslist'),
(106, 'Can view contract', 20, 'view_contract'),
(107, 'Can view django migrations', 8, 'view_djangomigrations'),
(108, 'Can view auth group', 2, 'view_authgroup'),
(109, 'Can view job status', 14, 'view_jobstatus'),
(110, 'Can view auth user', 15, 'view_authuser'),
(111, 'Can view django admin log', 7, 'view_djangoadminlog'),
(112, 'Can view sns list', 35, 'view_snslist'),
(113, 'Can view auth user user permissions', 13, 'view_authuseruserpermissions'),
(114, 'Can view keyword list', 21, 'view_keywordlist'),
(115, 'Can view sending failed users', 4, 'view_sendingfailedusers'),
(116, 'Can view auth group permissions', 12, 'view_authgrouppermissions'),
(117, 'Can view blacklist users', 1, 'view_blacklistusers'),
(118, 'Can view payment', 33, 'view_payment'),
(119, 'Can view company list', 30, 'view_companylist'),
(120, 'Can view django content type', 9, 'view_djangocontenttype'),
(121, 'Can view auth permission', 11, 'view_authpermission'),
(122, 'Can view service list', 34, 'view_servicelist'),
(123, 'Can view company info list', 5, 'view_companyinfolist'),
(124, 'Can view auth user groups', 23, 'view_authusergroups'),
(125, 'Can view authentication', 18, 'view_authentication'),
(126, 'Can view job list', 31, 'view_joblist'),
(127, 'Can view naver gathered users', 22, 'view_navergatheredusers'),
(128, 'Can view django session', 17, 'view_djangosession'),
(129, 'Can view naver compnay exgroup users', 19, 'view_navercompnayexgroupusers'),
(130, 'Can view managed exgroup users', 10, 'view_managedexgroupusers'),
(131, 'Can view reject users', 3, 'view_rejectusers'),
(132, 'Can view naver keyword job order', 16, 'view_naverkeywordjoborder'),
(133, 'Can view mb list', 32, 'view_mblist'),
(134, 'Can view naver company mailing users', 6, 'view_navercompanymailingusers'),
(135, 'Can view log entry', 24, 'view_logentry'),
(136, 'Can view permission', 26, 'view_permission'),
(137, 'Can view user', 27, 'view_user'),
(138, 'Can view group', 25, 'view_group'),
(139, 'Can view content type', 28, 'view_contenttype'),
(140, 'Can view session', 29, 'view_session');

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$120000$pSRmdhmeMypM$IjLenS4bOiae2yABNtq05gr0bCPyQuNWloNnG2DB3f0=', '2018-12-31 05:29:19', 1, 'admin', '', '', '', 1, 1, '2018-06-13 14:09:51'),
(2, 'pbkdf2_sha256$100000$VWYND3IUFgjk$mw5GLu72m2atITh7ZYh85eAdn+cAxVGuJBFACujAS+A=', NULL, 0, 'test', '', '', '', 0, 1, '2018-07-23 15:32:33');

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `company_list`
--

CREATE TABLE `company_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `mb_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT 'null',
  `charge_name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_mobile_phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'null',
  `homepage` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_date` datetime NOT NULL,
  `mb_list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `company_list`
--

INSERT INTO `company_list` (`id`, `mb_id`, `company_name`, `office_phone`, `charge_name`, `charge_mobile_phone`, `email`, `address`, `homepage`, `update_date`, `mb_list_id`) VALUES
(18, 'test14', 'brosi345', '2019941057', 'brosiidea', '2019941057', 'bigdatacop@gmail.com', '4 Medori Blvd, 2fasts 2m97397', '', '2018-12-27 04:24:18', 50),
(19, 'test10', 'sasdf', '2019941057', 'wereae', '2019941057', 'redhohoho@gmail.com', '4 Medori Blvd, 2fasts 2m97397', '', '2018-12-27 04:24:46', 46),
(20, 'test7', 'ast235wd', '2019941057', 'brosiidea', '2019941057', 'bluebamus@naver.com', '4 Medori Blvd, 2fasts 2m97397', '', '2018-12-27 04:49:07', 43);

-- --------------------------------------------------------

--
-- 테이블 구조 `contract`
--

CREATE TABLE `contract` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_type` tinyint(4) NOT NULL DEFAULT '0',
  `payment_date` datetime NOT NULL,
  `lately_payment_date` datetime DEFAULT NULL,
  `prepaid` tinyint(4) NOT NULL DEFAULT '0',
  `payment` int(11) NOT NULL,
  `contract_date` datetime NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `company_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `contract`
--

INSERT INTO `contract` (`id`, `service_type`, `payment_date`, `lately_payment_date`, `prepaid`, `payment`, `contract_date`, `start_date`, `end_date`, `finish_date`, `company_name`, `company_list_id`) VALUES
(1, 1, '2019-01-02 03:51:45', NULL, 1, 200, '2019-01-02 04:14:20', '2019-01-02 03:51:45', NULL, NULL, 'brosi345', 18);

-- --------------------------------------------------------

--
-- 테이블 구조 `contract_payment`
--

CREATE TABLE `contract_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `deposit_date` datetime NOT NULL,
  `deposit_amount` int(11) NOT NULL,
  `company_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `checker` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contract_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `object_id` longtext COLLATE utf8mb4_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-06-13 15:06:26', '1', 'MbList object (1)', 1, '[{"added": {}}]', 32, 1),
(2, '2018-07-23 15:32:34', '2', 'test', 1, '[{"added": {}}]', 27, 1),
(3, '2018-07-23 15:32:48', '2', 'test', 2, '[]', 27, 1),
(4, '2018-08-19 15:36:15', '2', 'MbList object (2)', 1, '[{"added": {}}]', 32, 1),
(5, '2018-09-20 23:03:01', '3', 'MbList object (3)', 3, '', 32, 1),
(6, '2018-12-16 15:54:12', '13', 'MbList object (13)', 1, '[{"added": {}}]', 32, 1),
(7, '2018-12-16 15:54:26', '13', 'MbList object (13)', 2, '[{"changed": {"fields": ["mb_id", "mb_hp"]}}]', 32, 1),
(8, '2018-12-16 15:54:35', '13', 'MbList object (13)', 2, '[]', 32, 1),
(9, '2018-12-16 15:54:43', '13', 'MbList object (13)', 2, '[{"changed": {"fields": ["mb_id"]}}]', 32, 1),
(10, '2018-12-16 15:54:58', '13', 'MbList object (13)', 2, '[]', 32, 1),
(11, '2018-12-16 15:55:41', '14', 'MbList object (14)', 1, '[{"added": {}}]', 32, 1);

-- --------------------------------------------------------

--
-- 테이블 구조 `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(24, 'admin', 'logentry'),
(25, 'auth', 'group'),
(26, 'auth', 'permission'),
(27, 'auth', 'user'),
(18, 'brosiidea_program', 'authentication'),
(2, 'brosiidea_program', 'authgroup'),
(12, 'brosiidea_program', 'authgrouppermissions'),
(11, 'brosiidea_program', 'authpermission'),
(15, 'brosiidea_program', 'authuser'),
(23, 'brosiidea_program', 'authusergroups'),
(13, 'brosiidea_program', 'authuseruserpermissions'),
(1, 'brosiidea_program', 'blacklistusers'),
(5, 'brosiidea_program', 'companyinfolist'),
(30, 'brosiidea_program', 'companylist'),
(20, 'brosiidea_program', 'contract'),
(7, 'brosiidea_program', 'djangoadminlog'),
(9, 'brosiidea_program', 'djangocontenttype'),
(8, 'brosiidea_program', 'djangomigrations'),
(17, 'brosiidea_program', 'djangosession'),
(31, 'brosiidea_program', 'joblist'),
(14, 'brosiidea_program', 'jobstatus'),
(21, 'brosiidea_program', 'keywordlist'),
(10, 'brosiidea_program', 'managedexgroupusers'),
(32, 'brosiidea_program', 'mblist'),
(6, 'brosiidea_program', 'navercompanymailingusers'),
(19, 'brosiidea_program', 'navercompnayexgroupusers'),
(22, 'brosiidea_program', 'navergatheredusers'),
(16, 'brosiidea_program', 'naverkeywordjoborder'),
(33, 'brosiidea_program', 'payment'),
(3, 'brosiidea_program', 'rejectusers'),
(4, 'brosiidea_program', 'sendingfailedusers'),
(34, 'brosiidea_program', 'servicelist'),
(35, 'brosiidea_program', 'snslist'),
(28, 'contenttypes', 'contenttype'),
(29, 'sessions', 'session');

-- --------------------------------------------------------

--
-- 테이블 구조 `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-06-12 00:34:17'),
(2, 'auth', '0001_initial', '2018-06-12 00:34:18'),
(3, 'admin', '0001_initial', '2018-06-12 00:34:19'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-06-12 00:34:19'),
(5, 'contenttypes', '0002_remove_content_type_name', '2018-06-12 00:34:19'),
(6, 'auth', '0002_alter_permission_name_max_length', '2018-06-12 00:34:19'),
(7, 'auth', '0003_alter_user_email_max_length', '2018-06-12 00:34:19'),
(8, 'auth', '0004_alter_user_username_opts', '2018-06-12 00:34:19'),
(9, 'auth', '0005_alter_user_last_login_null', '2018-06-12 00:34:19'),
(10, 'auth', '0006_require_contenttypes_0002', '2018-06-12 00:34:19'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2018-06-12 00:34:19'),
(12, 'auth', '0008_alter_user_username_max_length', '2018-06-12 00:34:20'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2018-06-12 00:34:20'),
(14, 'brosiidea_program', '0001_initial', '2018-06-12 00:34:20'),
(15, 'sessions', '0001_initial', '2018-06-12 00:34:20'),
(16, 'brosiidea_program', '0002_companylist_joblist_mblist_payment_servicelist_snslist', '2018-06-12 00:34:53'),
(17, 'admin', '0003_logentry_add_action_flag_choices', '2018-12-02 13:01:53');

-- --------------------------------------------------------

--
-- 테이블 구조 `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('01jq2ziycdft12z0sskcrqi7vnzy7hta', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2019-01-14 05:29:19'),
('1lagned795tz8rrw7wxa1mmcb5lnwjuw', 'NzM1YzBkNzZjM2NiNjM4NGM1ODdhODkzODc0ODY1ZDBmMjkwYjJiNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2018-12-16 16:23:33'),
('1yf33n61belkmlndr4i74n76qmdi23qk', 'NjIxZDVkYjI3MzE2MTczMGZkMDNkODYzMTM5OWRlNmJlZTliYjNkNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-12-20 15:31:59'),
('2biztjuyjk3829020pv85nnji2ekcxf4', 'NzBlMjU5ODcwMTIwYzYxNzA5ZGYzNmJmMGI3NWIzYjk2YjNjMDM4OTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQzZWRiNGE4ZjJmMTg0MmQwNTNkMTMzZmVjMGMyMGFlNzYyMjBhNzkiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-11-28 14:27:47'),
('3rfjufzk3w0ukmwt7qga7jzcf9h65ly4', 'MTk0ZTQ3YjljMjExOTFmNmEyMWQ5Zjg3MjM3N2JhZjI4ZTlhMmVjMzp7fQ==', '2018-11-23 08:09:48'),
('511xygwy04hr9b0eipeqc0l7cz4x3p0m', 'YmU4NTA4YmY1ZDNhOGIxODdhNmUyNGNmYTllZWNjMWVjNGQ4OGVhZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJkM2VkYjRhOGYyZjE4NDJkMDUzZDEzM2ZlYzBjMjBhZTc2MjIwYTc5In0=', '2018-11-28 04:22:26'),
('54vkq6pwozu4m4xlrfr4oa217n4pxonp', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-12-17 11:26:31'),
('5w74z7jut6peg4bi4457h48m3xwfxuyr', 'MjE4NjhmOGZmYTk3YTA5ZjQxMzM1MzMwYTk2NDdmZjAyMDY1Y2MwYzp7Il9hdXRoX3VzZXJfaGFzaCI6IjI3OGYyZjU2NDJhMjFjNGY5ZDk3ZjJjYjNhYTMxZjRmZDAzMGJjYWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-27 14:10:36'),
('6bvib9ehso7eas4y8sklbn9vuhz5f6w7', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2019-01-01 07:36:41'),
('6no8m67b2e9sshi0zggf5u71gxdg41bp', 'MTk0ZTQ3YjljMjExOTFmNmEyMWQ5Zjg3MjM3N2JhZjI4ZTlhMmVjMzp7fQ==', '2018-11-23 08:10:11'),
('70vuw4aym7953vref2cnbciotnped3y9', 'NzM1YzBkNzZjM2NiNjM4NGM1ODdhODkzODc0ODY1ZDBmMjkwYjJiNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2018-12-16 13:02:45'),
('8qf9fywt26fxwmas7a4sysbz3z8wrxhp', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-12-17 09:57:23'),
('a49kmmzotia7r12mu2b4pxto5xe9huc8', 'NzM4N2MyNmE0NWE0NmM3MWQ5NDljYzg5ZmE2ZmQ1ZDZiNDBlMTUxMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2018-12-20 01:46:41'),
('akv9vokvl3earbfbglpqti84d8n26yuu', 'OTM2NTdmZTU0N2ZkYjUxNjFmMzZiMTE4MGYxNjUyMGIyOGM4ZWYxYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDNlZGI0YThmMmYxODQyZDA1M2QxMzNmZWMwYzIwYWU3NjIyMGE3OSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-11-23 11:08:15'),
('ecm7m3e3cndfn6tvvlj1oqqeoyfymsqk', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-12-17 10:00:49'),
('fmearag9hmkbv4aj2xsjypr1gee9f5ym', 'NzM4N2MyNmE0NWE0NmM3MWQ5NDljYzg5ZmE2ZmQ1ZDZiNDBlMTUxMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2018-12-20 01:50:54'),
('h858xask0ycpihpvhk0n1nsufwjll6cb', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2019-01-10 04:39:56'),
('k9puuke7cez31u5eexjah934gtsyotjd', 'YTE1MzczMGJkZjU5NTE2OGFmYTMxYTk0ODc2YmY4NmQxMzMyMzQyNzp7Il9hdXRoX3VzZXJfaGFzaCI6ImNjYzAxNzMyZjI3YTdiYTY2ZGUwYTczM2I3N2ZjNjAzZmQwZDM3ZDUiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2019-01-07 05:01:24'),
('lnbce1t06ddwintl3mjk7mohbrok3wpn', 'NzI4OWJmZDU0ZTYwZmU3ZWQyNWYxNTE0OWVmMTVmNjA5MGJiNTQ3ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyNzhmMmY1NjQyYTIxYzRmOWQ5N2YyY2IzYWEzMWY0ZmQwMzBiY2FjIn0=', '2018-08-21 01:18:33'),
('morzdyarnh5r5mh0v2bx8sttzqyq31nd', 'NzI4OWJmZDU0ZTYwZmU3ZWQyNWYxNTE0OWVmMTVmNjA5MGJiNTQ3ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyNzhmMmY1NjQyYTIxYzRmOWQ5N2YyY2IzYWEzMWY0ZmQwMzBiY2FjIn0=', '2018-08-06 15:32:13'),
('mwaij9b7zew1jjzq5ksajmfjjxagex9f', 'ZGI4ZjZkYzIzNDNhY2RhNjAxMTZlZjczNmQwZmQ3Y2EwOTE1Y2IyYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjc4ZjJmNTY0MmEyMWM0ZjlkOTdmMmNiM2FhMzFmNGZkMDMwYmNhYyIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-07-09 12:46:27'),
('nghbwdi8ag5d32sci85mv4614g36i9t3', 'NzBlMjU5ODcwMTIwYzYxNzA5ZGYzNmJmMGI3NWIzYjk2YjNjMDM4OTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQzZWRiNGE4ZjJmMTg0MmQwNTNkMTMzZmVjMGMyMGFlNzYyMjBhNzkiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-11-26 03:55:03'),
('npuoigptlla0198jj0z4r3jx3qyrqmal', 'MmRkYmU1MmYwYzVkNzY3ZWJkYTI5ZTk3ZGNmMTI0M2ZiYzcyMjk3Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiY2NjMDE3MzJmMjdhN2JhNjZkZTBhNzMzYjc3ZmM2MDNmZDBkMzdkNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-12-17 09:57:54'),
('nzxz4ly5mnce30dbbyn1i6w6fi3p6msw', 'MzNkYTM4NTQ2MzlkMzBiMmYyM2E4OTc2MzA0M2QwNjA0MWJiOThlOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjI3OGYyZjU2NDJhMjFjNGY5ZDk3ZjJjYjNhYTMxZjRmZDAzMGJjYWMiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-10-03 05:46:38'),
('q4teh0gpuymg21j649irp3ma0lctak66', 'NzM4N2MyNmE0NWE0NmM3MWQ5NDljYzg5ZmE2ZmQ1ZDZiNDBlMTUxMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2018-12-31 01:40:20'),
('rbzize316m59fciwsasuzwg7zc3x39a9', 'MzA4M2FhMmRmMmM4Nzc5MjgyMzgwYWY0MzhiYzg3OGUyMTNjOTg2Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjc4ZjJmNTY0MmEyMWM0ZjlkOTdmMmNiM2FhMzFmNGZkMDMwYmNhYyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-09-02 15:32:27'),
('t2b6tem50mn1y2o8h5zkeokqvhio20dm', 'ZDNjZjcxMDJlNmE4MzA3YjZjNDRkNmFiYmYzZWMxZTJiNzVkNTUwYjp7Il9hdXRoX3VzZXJfaGFzaCI6ImQzZWRiNGE4ZjJmMTg0MmQwNTNkMTMzZmVjMGMyMGFlNzYyMjBhNzkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-11-28 06:34:13'),
('un2jsanh1wd5y40gb03yovw72fhx2jge', 'NTc0YzlkZTYyMjAxNDFmNTkzNjM2YWQxMGI0ZmZlZWU3MWQyMzg4MTp7Il9hdXRoX3VzZXJfaGFzaCI6ImNjYzAxNzMyZjI3YTdiYTY2ZGUwYTczM2I3N2ZjNjAzZmQwZDM3ZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-12-20 15:08:34'),
('vpa5jlc2qfqpk919bpqwghlwfyn8l1jk', 'OTM2NTdmZTU0N2ZkYjUxNjFmMzZiMTE4MGYxNjUyMGIyOGM4ZWYxYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDNlZGI0YThmMmYxODQyZDA1M2QxMzNmZWMwYzIwYWU3NjIyMGE3OSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2018-11-23 08:29:11'),
('w3kl0ikjw0zaczqz83sint4okzbht6z9', 'NzM4N2MyNmE0NWE0NmM3MWQ5NDljYzg5ZmE2ZmQ1ZDZiNDBlMTUxMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJjY2MwMTczMmYyN2E3YmE2NmRlMGE3MzNiNzdmYzYwM2ZkMGQzN2Q1In0=', '2019-01-09 16:00:04');

-- --------------------------------------------------------

--
-- 테이블 구조 `job_list`
--

CREATE TABLE `job_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_category_state` tinyint(5) NOT NULL DEFAULT '0',
  `job_number` tinyint(1) NOT NULL DEFAULT '0',
  `job_process_state` tinyint(1) NOT NULL DEFAULT '0',
  `insert_time` datetime NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `work_regularly` tinyint(1) NOT NULL DEFAULT '0',
  `dayofmonth` int(8) DEFAULT NULL,
  `dayofweek` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_time` datetime DEFAULT NULL,
  `auth_id` int(10) UNSIGNED NOT NULL,
  `service_list_id` int(10) UNSIGNED NOT NULL,
  `contract_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `mb_list`
--

CREATE TABLE `mb_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `mb_id` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_pwd` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_cash` int(11) DEFAULT '0',
  `mb_point` int(11) DEFAULT '0',
  `mb_name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_nick` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mb_email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_level` int(11) DEFAULT NULL,
  `mb_homepage` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mb_sex` tinyint(1) DEFAULT '0',
  `mb_birth` int(15) DEFAULT NULL,
  `mb_tel` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mb_hp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_login_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mb_leave_date` datetime DEFAULT NULL,
  `mb_email_certify` tinyint(1) DEFAULT '0',
  `mb_mailing` tinyint(1) DEFAULT '0',
  `mb_create_date` datetime NOT NULL,
  `mb_update_date` datetime NOT NULL,
  `company_state` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `mb_list`
--

INSERT INTO `mb_list` (`id`, `mb_id`, `mb_pwd`, `mb_cash`, `mb_point`, `mb_name`, `mb_nick`, `mb_email`, `mb_level`, `mb_homepage`, `mb_sex`, `mb_birth`, `mb_tel`, `mb_hp`, `mb_login_ip`, `mb_leave_date`, `mb_email_certify`, `mb_mailing`, `mb_create_date`, `mb_update_date`, `company_state`) VALUES
(37, 'test3', 'blue1324', NULL, NULL, 'Lim Do-Hyu', '', 'a@a.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-25 15:43:48', '2018-12-25 15:43:48', 0),
(38, 'test4', 'blue1324464', NULL, NULL, 'test2', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-25 15:44:03', '2018-12-25 15:44:03', 0),
(39, 'test1', 'blue1324', NULL, NULL, 'test1', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:52:22', '2018-12-26 15:57:38', 0),
(40, 'test2', 'blue1324', NULL, NULL, 'test1', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:52:38', '2018-12-26 15:52:38', 0),
(41, 'test5', 'blue1324', NULL, NULL, 'test1', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:52:56', '2018-12-26 15:52:56', 0),
(42, 'test6', 'blue1324', NULL, NULL, 'test1', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:53:27', '2018-12-26 15:53:27', 0),
(43, 'test7', 'test1', NULL, NULL, 'test2', '', 'a@a.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:54:23', '2018-12-27 04:49:07', 1),
(44, 'test8', 'blue1324464', NULL, NULL, 'test2', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:54:40', '2018-12-26 15:54:40', 0),
(45, 'test9', 'test1', NULL, NULL, 'test2', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:54:56', '2018-12-26 15:54:56', 0),
(46, 'test10', 'blue1324464', NULL, NULL, 'test2', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:55:13', '2018-12-27 04:24:46', 1),
(47, 'test11', 'test1', NULL, NULL, 'test2', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:55:33', '2018-12-27 04:59:37', 0),
(48, 'test12', 'blue1324464', NULL, NULL, 'Lim Do-Hyu', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:55:53', '2018-12-26 15:55:53', 0),
(49, 'test13', 'test1', NULL, NULL, '56', '', 'brosi@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-26 15:56:09', '2018-12-26 15:56:09', 0),
(50, 'test14', 'test1', NULL, NULL, 'test1', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:56:28', '2018-12-27 04:24:18', 1),
(51, 'test15', 'blue1324464', NULL, NULL, 'test2', '', 'a@a.com', NULL, '', NULL, NULL, '2019941057', '2019941057', '', NULL, NULL, NULL, '2018-12-26 15:56:44', '2018-12-26 15:57:56', 0),
(52, 'test16', 'blue1324', NULL, NULL, 'test2', '', 'Lim@brosi.com', NULL, '', NULL, NULL, '7146767274', '7146767274', '', NULL, NULL, NULL, '2018-12-27 04:40:18', '2018-12-27 04:40:18', 0);

-- --------------------------------------------------------

--
-- 테이블 구조 `payment`
--

CREATE TABLE `payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `payment_func` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'finish',
  `mb_list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `service_list`
--

CREATE TABLE `service_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `mb_list_id` int(10) UNSIGNED NOT NULL,
  `sns_list_id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_date` datetime NOT NULL,
  `ex_date` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `process_status` int(11) NOT NULL,
  `job_cnt` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 테이블 구조 `sns_list`
--

CREATE TABLE `sns_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `sns_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `authentication`
--
ALTER TABLE `authentication`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idauth_UNIQUE` (`id`);

--
-- 테이블의 인덱스 `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- 테이블의 인덱스 `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- 테이블의 인덱스 `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- 테이블의 인덱스 `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 테이블의 인덱스 `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- 테이블의 인덱스 `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- 테이블의 인덱스 `company_list`
--
ALTER TABLE `company_list`
  ADD PRIMARY KEY (`id`,`mb_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_company_list_mb_list1_idx` (`mb_list_id`);

--
-- 테이블의 인덱스 `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`id`,`company_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_contract_company_list1_idx` (`company_list_id`);

--
-- 테이블의 인덱스 `contract_payment`
--
ALTER TABLE `contract_payment`
  ADD PRIMARY KEY (`id`,`contract_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_contract_payment_contract1_idx` (`contract_id`) USING BTREE;

--
-- 테이블의 인덱스 `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk` (`user_id`);

--
-- 테이블의 인덱스 `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- 테이블의 인덱스 `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- 테이블의 인덱스 `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- 테이블의 인덱스 `job_list`
--
ALTER TABLE `job_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_job_list_auth1_idx` (`auth_id`),
  ADD KEY `fk_job_list_service_list1_idx` (`service_list_id`),
  ADD KEY `fk_job_list_contract1_idx` (`contract_id`);

--
-- 테이블의 인덱스 `mb_list`
--
ALTER TABLE `mb_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `mb_id_UNIQUE` (`mb_id`),
  ADD KEY `mb_id` (`mb_id`),
  ADD KEY `mb_hp` (`mb_hp`);

--
-- 테이블의 인덱스 `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`,`mb_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_payment_mb_list_idx` (`mb_list_id`);

--
-- 테이블의 인덱스 `service_list`
--
ALTER TABLE `service_list`
  ADD PRIMARY KEY (`id`,`mb_list_id`,`sns_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_service_list_mb_list1_idx` (`mb_list_id`),
  ADD KEY `fk_service_list_sns_list1_idx` (`sns_list_id`);

--
-- 테이블의 인덱스 `sns_list`
--
ALTER TABLE `sns_list`
  ADD PRIMARY KEY (`id`,`mb_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `sns_id_UNIQUE` (`sns_id`),
  ADD KEY `fk_sns_list_mb_list1_idx` (`mb_list_id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `authentication`
--
ALTER TABLE `authentication`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- 테이블의 AUTO_INCREMENT `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 테이블의 AUTO_INCREMENT `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `company_list`
--
ALTER TABLE `company_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- 테이블의 AUTO_INCREMENT `contract`
--
ALTER TABLE `contract`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 테이블의 AUTO_INCREMENT `contract_payment`
--
ALTER TABLE `contract_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- 테이블의 AUTO_INCREMENT `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- 테이블의 AUTO_INCREMENT `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- 테이블의 AUTO_INCREMENT `job_list`
--
ALTER TABLE `job_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `mb_list`
--
ALTER TABLE `mb_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- 테이블의 AUTO_INCREMENT `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `service_list`
--
ALTER TABLE `service_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `sns_list`
--
ALTER TABLE `sns_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 덤프된 테이블의 제약사항
--

--
-- 테이블의 제약사항 `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- 테이블의 제약사항 `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- 테이블의 제약사항 `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 테이블의 제약사항 `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 테이블의 제약사항 `company_list`
--
ALTER TABLE `company_list`
  ADD CONSTRAINT `fk_company_list_mb_list1` FOREIGN KEY (`mb_list_id`) REFERENCES `mb_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `fk_contract_company_list1` FOREIGN KEY (`company_list_id`) REFERENCES `company_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `contract_payment`
--
ALTER TABLE `contract_payment`
  ADD CONSTRAINT `fk_contract_payment_contract_list1` FOREIGN KEY (`contract_id`) REFERENCES `company_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 테이블의 제약사항 `job_list`
--
ALTER TABLE `job_list`
  ADD CONSTRAINT `fk_job_list_auth1` FOREIGN KEY (`auth_id`) REFERENCES `authentication` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_job_list_contract1` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_job_list_service_list1` FOREIGN KEY (`service_list_id`) REFERENCES `service_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_mb_list` FOREIGN KEY (`mb_list_id`) REFERENCES `mb_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `service_list`
--
ALTER TABLE `service_list`
  ADD CONSTRAINT `fk_service_list_mb_list1` FOREIGN KEY (`mb_list_id`) REFERENCES `mb_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_service_list_sns_list1` FOREIGN KEY (`sns_list_id`) REFERENCES `sns_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 테이블의 제약사항 `sns_list`
--
ALTER TABLE `sns_list`
  ADD CONSTRAINT `fk_sns_list_mb_list1` FOREIGN KEY (`mb_list_id`) REFERENCES `mb_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
