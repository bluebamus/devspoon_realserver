-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- 생성 시간: 19-01-03 11:39
-- 서버 버전: 10.1.25-MariaDB-1~xenial
-- PHP 버전: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `mt_solution`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `contract`
--

CREATE TABLE `contract` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_type` tinyint(4) NOT NULL DEFAULT '0',
  `payment_date` datetime NOT NULL,
  `lately_payment_date` datetime DEFAULT NULL,
  `prepaid` tinyint(4) NOT NULL DEFAULT '0',
  `payment` int(11) NOT NULL,
  `contract_date` datetime NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `company_name` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 테이블의 덤프 데이터 `contract`
--

INSERT INTO `contract` (`id`, `service_type`, `payment_date`, `lately_payment_date`, `prepaid`, `payment`, `contract_date`, `start_date`, `end_date`, `finish_date`, `company_name`, `company_list_id`) VALUES
(1, 1, '2019-01-02 03:51:45', NULL, 1, 200, '2019-01-02 04:14:20', '2019-01-02 03:51:45', NULL, NULL, 'brosi345', 18);

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`id`,`company_list_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_contract_company_list1_idx` (`company_list_id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `contract`
--
ALTER TABLE `contract`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 덤프된 테이블의 제약사항
--

--
-- 테이블의 제약사항 `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `fk_contract_company_list1` FOREIGN KEY (`company_list_id`) REFERENCES `company_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
