<?php /* Template_ 2.2.8 2016/11/03 01:16:55 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/layout/side_bs.html 000004093 */  $this->include_("eb_outlogin","eb_poll","eb_popular","eb_tagmenu","eb_visit");
$TPL_sidemenu_1=empty($TPL_VAR["sidemenu"])||!is_array($TPL_VAR["sidemenu"])?0:count($TPL_VAR["sidemenu"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가 ?>
<div class="basic-body-side <?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>left<?php }else{?>right<?php }?>-side col-md-3">
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_eyoom('notice_balloon','bo_table=게시판id||count=5||cut_subject=50')?>
</div>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_outlogin"]=='y'){?><?php echo outlogin('basic')?><?php }else{?><?php echo eb_outlogin($TPL_VAR["eyoom"]["outlogin_skin"])?><?php }?>
</div>
<?php if(!defined('_INDEX_')){?>
<div class="margin-bottom-20">
<div class="headline"><h6><strong>사이드 메뉴</strong></h6></div>
<ul class="list-group sidebar-nav-e1" id="sidebar-nav">
<?php if($TPL_sidemenu_1){foreach($TPL_VAR["sidemenu"] as $TPL_K1=>$TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
<li class="list-group-item list-toggle <?php if($TPL_V1["active"]){?>active<?php }?>">
<a <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-<?php echo $TPL_K1?>"<?php }else{?>href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>"<?php }?>><?php echo $TPL_V1["me_name"]?></a>
<ul id="collapse-<?php echo $TPL_K1?>" class="collapse <?php if($TPL_V1["active"]){?>in<?php }?>">
<?php if($TPL_submenu_2){foreach($TPL_V1["submenu"] as $TPL_V2){?>
<li class="<?php if($TPL_V2["active"]){?>active<?php }?>"><?php if($TPL_V2["new"]){?><span class="badge badge-red">new</span><?php }?><a href="<?php echo $TPL_V2["me_link"]?>" target="_<?php echo $TPL_V2["me_target"]?>"><?php if($TPL_V2["active"]){?><i class="fa fa-chevron-circle-right"></i><?php }else{?><i class="fa fa-circle"></i><?php }?> <?php echo $TPL_V2["me_name"]?></a></li>
<?php }}?>
</ul>
</li>
<?php }}?>
</ul>
</div>
<?php }?>
<div class="margin-bottom-20">
<div class="side-tab margin-bottom-20">
<div class="tab-e2">
<ul class="nav nav-tabs">
<li class="active"><a href="#side-tn-1" data-toggle="tab">새글</a></li>
<li class="last"><a href="#side-tn-2" data-toggle="tab">새댓글</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="side-tn-1">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_newpost('tab_newpost','count=5||cut_subject=30||photo=y')?>
</div>
</div>
<div class="tab-pane fade in" id="side-tn-2">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_newpost('tab_newcomment','count=5||cut_subject=30||photo=y')?>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_poll"]=='y'){?><?php echo poll('basic')?><?php }else{?><?php echo eb_poll($TPL_VAR["eyoom"]["poll_skin"])?><?php }?>
</div>
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_rankset('basic','10')?>
</div>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_popular"]=='y'){?><?php echo popular('basic')?><?php }else{?><?php echo eb_popular($TPL_VAR["eyoom"]["popular_skin"])?><?php }?>
</div>
<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'){?>
<div class="margin-bottom-20">
<?php echo eb_tagmenu($TPL_VAR["eyoom"]["tag_skin"])?>
</div>
<?php }?>
<?php if($GLOBALS["is_admin"]){?>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_visit"]=='y'){?><?php echo visit('basic')?><?php }else{?><?php echo eb_visit($TPL_VAR["eyoom"]["visit_skin"])?><?php }?>
</div>
<?php }?>
</div>
<?php if($TPL_VAR["is_side_sticky"]=='yes'){?>
<script type="text/javascript" src="/eyoom/theme/basic2/js/theia-sticky-sidebar.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.initSideSticky();
});
</script>
<?php }?>