<?php /* Template_ 2.2.8 2020/03/31 21:20:07 /www/infra.devspoon/eyoom/theme/basic2/layout/tail_bs.html 000004583 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가 ?>
<?php if(!$GLOBALS["wmode"]){?>
</div>
<?php if((defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_main_side_layout"]=='y')||(!defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_sub_side_layout"]=='y'&&$TPL_VAR["subinfo"]["sidemenu"]!='n')){?>
<?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='right'){?>
<?php $this->print_("side_bs",$TPL_SCP,1);?>
<?php }?>
<?php }?>
<div class="clearfix"></div>
</div>	    </div>
<?php if($TPL_VAR["color"]=='white'){?>
<div class="footer footer-light">
<?php }elseif($TPL_VAR["color"]=='black'){?>
<div class="footer footer-dark">
<?php }?>
<div class="container">
<div class="row">
<div class="col-md-3 sm-margin-bottom-30">
<div class="heading-footer"><h4>About Us</h4></div>
<p class="margin-bottom-15">Lorem ipsum dulor sit amet, consectetur adipisicing elft. Harum dolore, sequi ex quam sunt delectus veniam aut tempore illum, dulor nemo quae nulla nam perferendis enim quisquam, culpa.</p>
<ul class="social-icons">
<li><a href="#" class="social_facebook"></a></li>
<li><a href="#" class="social_twitter"></a></li>
<li><a href="#" class="social_google"></a></li>
<li><a href="#" class="social_kakaostory"></a></li>
<li><a href="#" class="social_band"></a></li>
<li><a href="#" class="social_youtube"></a></li>
<li><a href="#" class="social_instagram"></a></li>
<li><a href="#" class="social_pinterest"></a></li>
<li><a href="#" class="social_behance"></a></li>
<li><a href="#" class="social_tumblr"></a></li>
</ul>
</div>
<div class="col-md-3 sm-margin-bottom-30">
<div class="heading-footer"><h4>Recent Notice</h4></div>
<?php echo $TPL_VAR["latest"]->latest_eyoom('footer_latest','bo_table=게시판id||count=3||cut_subject=40')?>
</div>
<div class="col-md-3 sm-margin-bottom-30">
<div class="heading-footer"><h4>Information</h4></div>
<ul class="list-unstyled footer-link-list">
<li><a href="<?php echo G5_URL?>/page/?pid=aboutus">About Us</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=provision">이용약관</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=privacy">개인정보 취급방침</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=noemail">이메일 무단수집거부</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=contactus">Contact Us</a></li>
</ul>
</div>
<div class="col-md-3">
<div class="heading-footer"><h4>Contact Us</h4></div>
<ul class="list-unstyled contactus">
<li>
<i class="fa fa-map-marker"></i>
서울시 OOO OOO OOO<br>
000-000
</li>
<li>
<i class="fa fa-phone"></i>
TEL : (02) OOO OOOO<br>
FAX : (02) OOO OOOO
</li>
<li>
<i class="fa fa-globe"></i>
<a href="mailto:webmaster@site.name">webmaster@site.name</a><br>
<a href="#">http://사이트주소</a>
</li>
</ul>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
<p class="text-center">Copyright &copy; <?php echo $TPL_VAR["config"]["cf_title"]?>. All Rights Reserved.</p>
</div>
</div>
</div>
</div></div><?php }?>
<?php if($GLOBALS["is_admin"]&&!G5_IS_MOBILE&&!$_GET["wmode"]){?>
<?php $this->print_("misc_bs",$TPL_SCP,1);?>
<?php }?>
<script type="text/javascript" src="/eyoom/theme/basic2/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/js/jquery.bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/js/jquery.sidebar.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/js/back-to-top.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/eyoom-form/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/eyoom-form/plugins/jquery-form/jquery.form.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/js/app.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
});
</script>
<!--[if lt IE 9]>
<script src="/eyoom/theme/basic2/js/respond.js"></script>
<script src="/eyoom/theme/basic2/js/html5shiv.js"></script>
<script src="/eyoom/theme/basic2/plugins/eyoom-form/js/eyoom-form-ie8.js"></script>
<![endif]-->
<?php $this->print_("tail_sub",$TPL_SCP,1);?>