<?php /* Template_ 2.2.8 2020/03/31 21:20:07 /www/infra.devspoon/eyoom/theme/basic2/layout/head_bs.html 000015326 */  $this->include_("eb_tagrecommend");
$TPL_menu_1=empty($TPL_VAR["menu"])||!is_array($TPL_VAR["menu"])?0:count($TPL_VAR["menu"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/owl-carousel/owl-carousel/owl.carousel.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/custom.css" type="text/css" media="screen">',0);
?>
<?php
// ------------- 테마 디자인 설정 옵션 시작 ------------- //
/**
* Design Layout 종류 : wide or boxed
*/
$TPL_VAR["layout"] = 'wide';
/**
* Design Color 종류 : white or black
*/
$TPL_VAR["color"] = 'white';
/**
* Mega Menu 설정 : yes or no
*/
$TPL_VAR["is_megamenu"] = 'no';
/**
* Side Sticky 설정 : yes or no
*/
$TPL_VAR["is_side_sticky"] = 'yes';
/**
* Header Slider 설정 : yes or no
*/
$TPL_VAR["is_slider"] = 'no';
/**
* Logo Type 종류 : image or text
*/
$TPL_VAR["logo"] = 'text';
/**
* Header Banner 종류 : image or text
*/
$TPL_VAR["hbanner"] = 'text';
// ------------- 테마 디자인 설정 옵션 끝 ------------- //
?>
<?php if(!$GLOBALS["wmode"]){?>
<div class="wrapper">
<?php if($TPL_VAR["layout"]=='wide'){?>
<div class="header-fixed basic-layout">
<?php }elseif($TPL_VAR["layout"]=='boxed'){?>
<div class="header-fixed boxed-layout container">
<?php }?>
<div class="header-topbar">
<div class="container">
<div class="row">
<div class="col-md-6">
<ul class="list-unstyled topbar-left">
<?php if(!G5_IS_MOBILE){?>
<li>
<a id="bookmarkme" href="javascript:void(0);" rel="sidebar" title="bookmark this page">북마크</a>
<script>
$(function() {
$("#bookmarkme").click(function() {
// Mozilla Firefox Bookmark
if ('sidebar' in window && 'addPanel' in window.sidebar) {
window.sidebar.addPanel(location.href,document.title,"");
} else if( /*@cc_on!@*/false) { // IE Favorite
window.external.AddFavorite(location.href,document.title);
} else { // webkit - safari/chrome
alert('단축키 ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command' : 'CTRL') + ' + D를 눌러 북마크에 추가하세요.');
}
});
});
</script>
</li>
<?php }?>
<li>
<a href="<?php echo G5_BBS_URL?>/current_connect.php">접속자 <?php echo $TPL_VAR["connect"]["total_cnt"]?><?php if($TPL_VAR["connect"]["mb_cnt"]){?> (<span><?php echo $TPL_VAR["connect"]["mb_cnt"]?></span>)<?php }?></a>
</li>
<li><a href="<?php echo G5_BBS_URL?>/faq.php">FAQ</a></li>
<li><a href="<?php echo G5_BBS_URL?>/qalist.php">1:1문의</a></li>
<li><a href="<?php echo G5_BBS_URL?>/new.php">새글</a></li>
<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'){?>
<li>
<?php echo eb_tagrecommend($TPL_VAR["eyoom"]["tag_skin"])?>
</li>
<?php }?>
</ul>
</div>
<div class="col-md-6">
<ul class="list-unstyled topbar-right">
<?php if($GLOBALS["is_member"]){?>
<?php if($GLOBALS["is_admin"]){?>
<li><a href="<?php echo G5_ADMIN_URL?>">관리자</a></li>
<?php }?>
<li class="hidden-xs"><a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=<?php echo G5_BBS_URL?>/register_form.php">정보수정</a></li>
<li><a href="<?php echo G5_BBS_URL?>/logout.php">로그아웃</a></li>
<?php echo $TPL_VAR["latest"]->latest_memo('memo_latest','count=8||cut_subject=20||photo=y')?>
<?php echo $TPL_VAR["latest"]->latest_respond('respond_latest','count=8||cut_subject=20||photo=y')?>
<?php }else{?>
<li><a href="<?php echo G5_BBS_URL?>/register.php">회원가입</a></li>
<li><a href="<?php echo G5_BBS_URL?>/login.php">로그인</a></li>
<?php }?>
</ul>
</div>
</div>
</div>
</div>
<div class="header-title">
<div class="container">
<div class="header-logo">
<?php if($TPL_VAR["logo"]=='text'){?>
<a class="navbar-brand" href="<?php echo G5_URL?>"><?php echo $TPL_VAR["config"]["cf_title"]?></a>
<?php }elseif($TPL_VAR["logo"]=='image'){?>
<a class="navbar-brand" href="<?php echo G5_URL?>"><img src="/eyoom/theme/basic2/image/site_logo.png" class="img-responsive" alt="<?php echo $TPL_VAR["config"]["cf_title"]?> LOGO"></a>
<?php }?>
</div>
<div class="clearfix"></div>
</div>
</div>
<?php if($TPL_VAR["color"]=='white'){?>
<div class="header-nav nav-background-light header-sticky">
<?php }elseif($TPL_VAR["color"]=='black'){?>
<div class="header-nav nav-background-dark header-sticky">
<?php }?>
<div class="navbar mega-menu" role="navigation">
<div class="container">
<div class="menu-container">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-align-justify"></span>
</button>
<div class="nav-in-right">
<ul class="menu-icons-list">
<li class="menu-icons">
<i class="menu-icons-style search search-close search-btn fa fa-search"></i>
</li>
</ul>
</div>
</div>
<div class="menu-container">
<div class="search-open">
<form name="fsearchbox" method="get" action="<?php echo G5_BBS_URL?>/search.php" onsubmit="return fsearchbox_submit(this);">
<input type="hidden" name="sfl" value="wr_subject||wr_content">
<input type="hidden" name="sop" value="and">
<label for="sch_stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="stx" id="sch_stx" maxlength="20" class="form-control" class="form-control" placeholder="전체검색... [검색어를 입력하세요]">
</form>
<script>
function fsearchbox_submit(f) {
if (f.stx.value.length < 2 || f.stx.value == $("#sch_stx").attr("placeholder")) {
alert("검색어는 두글자 이상 입력하십시오.");
f.stx.select();
f.stx.focus();
return false;
}
var cnt = 0;
for (var i=0; i<f.stx.value.length; i++) {
if (f.stx.value.charAt(i) == ' ') cnt++;
}
if (cnt > 1) {
alert("빠른 검색을 위하여 검색어에 공백은 한개만 입력할 수 있습니다.");
f.stx.select();
f.stx.focus();
return false;
}
return true;
}
</script>
</div>
</div>
<div class="collapse navbar-collapse navbar-responsive-collapse">
<div class="menu-container">
<ul class="nav navbar-nav">
<li class="<?php if(defined('_INDEX_')){?>active<?php }?>">
<a href="<?php echo G5_URL?><?php if($GLOBALS["is_member"]&&$TPL_VAR["eyoomer"]["main_index"]!='index'){?>/?home<?php }?>">HOME</a>
</li>
<?php if($TPL_menu_1){foreach($TPL_VAR["menu"] as $TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
<li class="<?php if($TPL_V1["active"]){?>active<?php }?> <?php if($TPL_V1["submenu"]){?>dropdown<?php }?>">
<a href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>" class="dropdown-toggle" <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="dropdown"<?php }else{?>data-hover="dropdown"<?php }?>>
<?php if($TPL_V1["me_icon"]){?><i class="fa <?php echo $TPL_V1["me_icon"]?>"></i> <?php }?><?php echo $TPL_V1["me_name"]?><?php if($TPL_V1["new"]){?>&nbsp;<i class="fa fa-check-circle color-red"></i><?php }?>
</a>
<?php if($TPL_submenu_2){$TPL_I2=-1;foreach($TPL_V1["submenu"] as $TPL_V2){$TPL_I2++;
$TPL_subsub_3=empty($TPL_V2["subsub"])||!is_array($TPL_V2["subsub"])?0:count($TPL_V2["subsub"]);?>
<?php if($TPL_I2== 0){?>
<ul class="dropdown-menu">
<?php }?>
<li class="dropdown-submenu <?php if($TPL_V2["active"]){?>active<?php }?>">
<a href="<?php echo $TPL_V2["me_link"]?>" target="_<?php echo $TPL_V2["me_target"]?>"><?php if($TPL_V2["me_icon"]){?><i class="fa <?php echo $TPL_V2["me_icon"]?>"></i> <?php }?><?php echo $TPL_V2["me_name"]?><?php if($TPL_V2["new"]){?>&nbsp;<i class="fa fa-check-circle color-red"></i><?php }?><?php if($TPL_V2["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
<?php if($TPL_subsub_3){$TPL_I3=-1;foreach($TPL_V2["subsub"] as $TPL_V3){$TPL_I3++;?>
<?php if($TPL_I3== 0){?>
<ul class="dropdown-menu <?php if($TPL_V3["active"]){?>active<?php }?>">
<?php }?>
<li class="dropdown-submenu">
<a href="<?php echo $TPL_V3["me_link"]?>" target="_<?php echo $TPL_V3["me_target"]?>"><?php if($TPL_V3["me_icon"]){?><i class="fa <?php echo $TPL_V3["me_icon"]?>"></i> <?php }?><?php echo $TPL_V3["me_name"]?><?php if($TPL_V3["new"]){?>&nbsp;<i class="fa fa-check-circle color-red"></i><?php }?><?php if($TPL_V3["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
</li>
<?php if($TPL_I3==$TPL_subsub_3- 1){?>
</ul>
<?php }?>
<?php }}?>
</li>
<?php if($TPL_I2==$TPL_submenu_2- 1){?>
</ul>
<?php }?>
<?php }}?>
</li>
<?php }}?>
<?php if($TPL_VAR["is_megamenu"]=='yes'){?>
<li class="dropdown mega-menu-area">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
전체메뉴
</a>
<ul class="dropdown-menu">
<li>
<div class="mega-menu-content disable-icons">
<div class="container">
<div class="row mega-height">
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
</div>
</div>
</div>
</li>
</ul>
</li>
<?php }?>
</ul>
</div>
</div>	            </div>
</div>
</div>
<div class="header-sticky-space"></div>
<?php if(defined('_INDEX_')||defined('_EYOOM_MYPAGE_')){?>
<?php if($TPL_VAR["is_slider"]=='yes'){?>
<div class="header-slider">
<div id="owl-header-slider" class="owl-carousel owl-theme">
<div class="item header-slider-bg">
<img src="/eyoom/theme/basic2/image/header_slider/slider_1.jpg" class="img-responsive">
<div class="container">
<h1>Welcome to <?php echo $TPL_VAR["config"]["cf_title"]?></h1>
<h2><?php echo $TPL_VAR["config"]["cf_title"]?> 을 방문해 주셔서 감사합니다.</h2>
<p>Lorem ipsum dulor sit amet.<br>Consectetur adipisicing elft. Harum dolore, sequi ex quam sunt delectus veniam aut tempore illum, dulor nemo quae nulla.<br>Nam perferendis enim quisquam, culpa.</p>
</div>
</div>
<div class="item header-slider-bg">
<img src="/eyoom/theme/basic2/image/header_slider/slider_2.jpg" class="img-responsive">
<div class="container">
<h1>Have a good time</h1>
<p><br>Lorem ipsum dulor sit amet.<br>Consectetur adipisicing elft. Harum dolore, sequi ex quam sunt delectus veniam aut tempore illum, dulor nemo quae nulla.<br>Nam perferendis enim quisquam, culpa.</p>
<div class="text-center margin-top-10">
<a href="<?php echo G5_URL?>/page/?pid=aboutus" class="btn-e btn-e-yellow btn-e-lg">About Us</a>
</div>
</div>
</div>
</div>
</div>
<?php }?>
<?php }else{?>
<div class="board-title">
<div class="container">
<h3 class="pull-left"><i class="fa fa-map-marker"></i> <?php echo $TPL_VAR["subinfo"]["title"]?></h3>
<ul class="pull-right breadcrumb">
<?php echo $TPL_VAR["subinfo"]["path"]?>
</ul>
</div>
</div>
<?php }?>
<div class="basic-body container">
<div class="row">
<?php if((defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_main_side_layout"]=='y')||(!defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_sub_side_layout"]=='y'&&$TPL_VAR["subinfo"]["sidemenu"]!='n')){?>
<?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>
<?php $this->print_("side_bs",$TPL_SCP,1);?>
<?php }?>
<div class="basic-body-main <?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>right<?php }else{?>left<?php }?>-main col-md-9">
<?php }else{?>
<div class="basic-body-main col-md-12">
<?php }?>
<?php }?>