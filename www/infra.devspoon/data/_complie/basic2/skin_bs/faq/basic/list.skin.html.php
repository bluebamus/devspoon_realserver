<?php /* Template_ 2.2.8 2017/07/06 22:45:41 /home/tripfooter.com/www/eyoom/theme/basic2/skin_bs/faq/basic/list.skin.html 000004928 */  $this->include_("eb_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);
$TPL__faq_list_1=empty($GLOBALS["faq_list"])||!is_array($GLOBALS["faq_list"])?0:count($GLOBALS["faq_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;?>
<div class="faq-wrap">
<?php if($GLOBALS["himg_src"]){?>
<div id="faq_himg" class="faq_img"><img class="img-responsive" src="<?php echo $GLOBALS["himg_src"]?>" alt=""></div>
<?php }?>
<div id="faq_hhtml" class="margin-bottom-10"><?php echo stripslashes($TPL_VAR["fm"]["fm_head_html"])?></div>
<div class="tab-e1">
<?php if(count($GLOBALS["faq_master_list"])){?>
<ul class="nav nav-tabs">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<li <?php if($TPL_V1["fm_id"]==$GLOBALS["fm_id"]){?>class="active"<?php }?>><a href="<?php echo $TPL_V1["category_href"]?>?fm_id=<?php echo $TPL_V1["fm_id"]?>" <?php echo $TPL_V1["category_option"]?>><?php echo $TPL_V1["category_msg"]?><?php echo $TPL_V1["fm_subject"]?></a></li>
<?php }}?>
</ul>
<?php }?>
<div class="margin-bottom-10"></div>
<div id="faq_wrap" class="faq_<?php echo $GLOBALS["fm_id"]?>">
<?php if(count($GLOBALS["faq_list"])){?>
<section id="faq_con">
<h2><?php echo $TPL_VAR["g5"]["title"]?> 목록</h2>
<ol>
<?php if($TPL__faq_list_1){foreach($GLOBALS["faq_list"] as $TPL_V1){?>
<li>
<h5><a href="#none" onclick="return faq_open(this);"><?php echo conv_content($TPL_V1["fa_subject"], 1)?></a></h5>
<div class="con_inner">
<?php echo conv_content($TPL_V1["fa_content"], 1)?>
<div class="con_closer"><button type="button" class="closer_btn"><span class="btn-e btn-e-default btn-e-sm">닫기</span></button></div>
</div>
</li>
<?php }}?>
</ol>
</section>
<?php }else{?>
<?php if($GLOBALS["stx"]){?>
<p class="empty_list">검색된 게시물이 없습니다.</p>
<?php }else{?>
<div class="text-center margin-top-30">
등록된 FAQ가 없습니다.
<?php if($GLOBALS["is_admin"]){?><br><a href="<?php echo G5_ADMIN_URL?>/faqmasterlist.php">FAQ를 새로 등록하시려면 FAQ관리</a> 메뉴를 이용하십시오.<?php }?>
</div>
<?php }?>
<?php }?>
</div>
</div>
<?php echo eb_paging('basic')?>
<div id="faq_thtml" class="margin-top-10"><?php echo stripslashes($TPL_VAR["fm"]["fm_tail_html"])?></div>
<?php if($GLOBALS["timg_src"]){?>
<div id="faq_timg" class="faq_img"><img src="<?php echo $GLOBALS["timg_src"]?>" alt=""></div>
<?php }?>
<form name="faq_search_form" method="get" class="eyoom-form">
<input type="hidden" name="fm_id" value="<?php echo $GLOBALS["fm_id"]?>">
<div class="row">
<section class="col col-4"></section>
<section class="col col-4">
<div class="input-group">
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<label class="input">
<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" required id="stx" class="form-control" size="15" maxlength="15" placeholder="FAQ검색">
</label>
<span class="input-group-btn">
<button class="btn btn-default btn-e-group" type="submit" value="검색">검색</button>
</span>
</div>
</section>
<?php if($GLOBALS["admin_href"]){?>
<section class="col col-4 text-right">
<a href="<?php echo $GLOBALS["admin_href"]?>" class="btn-e btn-e-red">FAQ 수정</a>
</section>
<?php }?>
</div>
</form>
</div>
<style>
.margin-hr-10 {height:1px;border-top:1px dotted #ddd;margin:10px 0}
.faq-wrap {overflow:hidden}
#faq_wrap {margin:0;}
#faq_wrap h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
#faq_wrap ol {margin:0;padding:0;list-style:none}
#faq_con {border:1px solid #e5e5e5;border-top:0}
#faq_con h5 {margin-top:0;margin-bottom:0;font-size:12px}
#faq_con h5 a {display:block;padding:5px 10px;border-top:1px solid #e5e5e5;background:#f8f8f8;text-decoration:none}
#faq_con h5 p {margin-bottom:0}
#faq_con .con_inner {display:none;padding:10px;line-height:1.5;border-top:1px solid #e5e5e5}
#faq_con .con_closer {margin:10px 0 0;text-align:right}
#faq_con .closer_btn {margin:0;padding:0;border:0;background:transparent}
.faq_tolist {padding:0 10px;text-align:right}
.faq_img {text-align:center}
</style>
<script src="/js/viewimageresize.js"></script>
<script>
$(function() {
$(".closer_btn").on("click", function() {
$(this).closest(".con_inner").slideToggle();
});
$("#faq_hhtml img").addClass("img-responsive");
$("#faq_hhtml img").attr("style",'');
$("#faq_thtml img").addClass("img-responsive");
$("#faq_thtml img").attr("style",'');
$("#faq_con img").addClass("img-responsive");
$("#faq_con img").attr("style",'');
});
function faq_open(el) {
var $con = $(el).closest("li").find(".con_inner");
if($con.is(":visible")) {
$con.slideUp();
} else {
$("#faq_con .con_inner:visible").css("display", "none");
$con.slideDown(
function() {
// 이미지 리사이즈
$con.viewimageresize2();
}
);
}
return false;
}
</script>