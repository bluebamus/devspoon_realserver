<?php /* Template_ 2.2.8 2017/05/18 19:13:12 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/skin_bs/member/basic/memo.skin.html 000004684 */  $this->include_("eb_nameview");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/css/custom.css" type="text/css" media="screen">',0);
?>
<div class="memo-list">
<h5 class="margin-bottom-20"><strong><?php echo $TPL_VAR["g5"]["title"]?></strong></h5>
<div class="tab-e1 margin-bottom-20">
<ul class="nav nav-tabs">
<li <?php if($GLOBALS["kind"]=='recv'){?>class="active"<?php }?>><a href="./memo.php?kind=recv">받은쪽지</a></li>
<li <?php if($GLOBALS["kind"]=='send'){?>class="active"<?php }?>><a href="./memo.php?kind=send">보낸쪽지</a></li>
<li><a href="./memo_form.php">쪽지쓰기</a></li>
</ul>
<div class="tab-content">
<div class="note margin-bottom-10">전체 <?php echo $GLOBALS["kind_title"]?>쪽지: <strong class="color-red"><?php echo $GLOBALS["total_count"]?></strong>통</div>
<!-- 쪽지 목록 시작 -->
<div class="table-list-eb">
<div class="board-list-body">
<table class="table table-hover">
<thead>
<tr>
<th><?php if($GLOBALS["kind"]=='recv'){?>보낸사람<?php }else{?>받는사람<?php }?></th>
<th>내용</th>
<th class="memo-hidden-sm">보낸시간</th>
<th class="memo-hidden-sm">읽은시간</th>
<th class="memo-hidden-sm">관리</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<tr>
<td class="text-center"><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["mb_nick"],$TPL_V1["mb_email"],$TPL_V1["mb_homepage"])?></td>
<td class="text-center"><a href="<?php echo $TPL_V1["view_href"]?>" class="btn-e btn-e-default btn-e-xs color-white <?php if($TPL_V1["read_datetime"]=='아직 읽지 않음'){?>btn-e-red<?php }?>">쪽지 보기</a></td>
<td class="text-center memo-hidden-sm"><a href="<?php echo $TPL_V1["view_href"]?>"><?php echo $TPL_V1["send_datetime"]?></a></td>
<td class="text-center memo-hidden-sm"><a href="<?php echo $TPL_V1["view_href"]?>"><?php echo $TPL_V1["read_datetime"]?></a></td>
<td class="text-center memo-hidden-sm"><a href="<?php echo $TPL_V1["del_href"]?>" onclick="del(this.href); return false;">삭제</a></td>
</tr>
<tr class="td-mobile memo-hidden-lg">						        <td colspan="2">
<span>[보낸시간] <strong class="color-black"><?php echo $TPL_V1["send_datetime"]?></strong></span>
<span>[읽은시간] <strong class="color-black"><?php echo $TPL_V1["read_datetime"]?></strong></span>
<span class="pull-right"><a href="<?php echo $TPL_V1["del_href"]?>" onclick="del(this.href); return false;">삭제</a></span>
</td>
</tr>
<?php }}else{?>
<tr><td colspan="5" class="text-center">자료가 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<!-- 쪽지 목록 끝 -->
</div>
</div>
<div class="note margin-bottom-10 font-size-11"><strong>Note:</strong> 쪽지 보관일수는 최장 <strong><?php echo $TPL_VAR["config"]["cf_memo_del"]?></strong>일 입니다.</div>
<div class="text-center margin-bottom-50">
<button type="button" onclick="window.close();" class="btn-e btn-e-dark">창닫기</button>
</div>
</div>
<style>
.margin-hr-10 {height:1px;border-top:1px dotted #ddd;margin:10px 0}
.memo-list {padding:15px;font-size:12px}
.memo-list .memo-hidden-lg {display:none}
@media (max-width: 500px) {
.memo-list .memo-hidden-sm {display:none}
.memo-list .memo-hidden-lg {display:table-row !important}
}
.table-list-eb .table thead > tr > th {border-bottom:1px solid #000}
.table-list-eb .table tbody > tr > td {padding:8px 5px}
.table-list-eb .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {background:#fafafa}
.table-list-eb thead {border-top:1px solid #000;border-bottom:1px solid #000;background:#fff}
.table-list-eb th {color:#000;font-weight:bold;white-space:nowrap}
.table-list-eb .td-mobile td {border-top:1px solid #f0f0f0;padding:4px 5px !important;font-size:10px;color:#999;background:#fcfcfc}
.table-list-eb .td-mobile td span {margin-right:5px}
</style>
<?php $this->print_("tail_sub",$TPL_SCP,1);?>