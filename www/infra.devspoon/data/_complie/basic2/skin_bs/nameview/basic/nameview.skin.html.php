<?php /* Template_ 2.2.8 2020/03/31 21:22:41 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/nameview/basic/nameview.skin.html 000002223 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<span class="sv_wrap">
<?php if(!$TPL_VAR["is_anonymous"]){?>
<a href="<?php echo $TPL_VAR["head"]["link"]?>" class="sv_member" title="<?php echo $TPL_VAR["head"]["title"]?>" target="_blank" onclick="return false;"> <b><?php echo $TPL_VAR["head"]["name"]?></b></a>
<?php }else{?>
<b><?php echo $TPL_VAR["head"]["name"]?></b>
<?php }?>
<?php  ob_start(); ?>
<?php if(!$TPL_VAR["is_anonymous"]){?>
<span class="sv dropdown-menu" role="menu">
<?php if($TPL_VAR["mb_id"]&&$TPL_VAR["member"]["mb_id"]){?>
<a href="<?php echo G5_BBS_URL?>/memo_form.php?me_recv_mb_id=<?php echo $TPL_VAR["mb_id"]?>" class="win_memo" id="ol_after_memo">쪽지보내기</a>
<a href="<?php echo $TPL_VAR["link"]["profile"]?>" onclick="win_profile(this.href); return false;">자기소개</a>
<a href="<?php echo $TPL_VAR["link"]["article"]?>">전체게시물</a>
<?php }?>
<?php if($TPL_VAR["email"]){?>
<a href="<?php echo $TPL_VAR["link"]["email"]?>" onclick="win_email(this.href); return false;">메일보내기</a>
<?php }?>
<?php if($TPL_VAR["home"]){?>
<a href="<?php echo $TPL_VAR["link"]["home"]?>" target="_blank">홈페이지</a>
<?php }?>
<?php if($TPL_VAR["bo_table"]){?>
<?php if($TPL_VAR["mb_id"]){?>
<a href="<?php echo $TPL_VAR["link"]["sid"]?>">아이디로 검색</a>
<?php }else{?>
<a href="<?php echo $TPL_VAR["link"]["sname"]?>">이름으로 검색</a>
<?php }?>
<?php }?>
<?php if($TPL_VAR["g5"]["sms5_use_sideview"]){?>
<a href="<?php echo $TPL_VAR["link"]["sms"]?>" class="win_sms5" target="_blank">문자보내기</a>
<?php }?>
<?php if($TPL_VAR["is_admin"]){?>
<a href="<?php echo $TPL_VAR["link"]["info"]?>" target="_blank">회원정보변경</a>
<a href="<?php echo $TPL_VAR["link"]["point"]?>" target="_blank"><?php echo $TPL_VAR["levelset"]["gnu_name"]?>내역</a>
<?php }?>
</span>
<?php }?>
<?php
$mb_name = ob_get_contents();
ob_end_clean();
?>
<?php echo $mb_name; ?>
<?php if(!$TPL_VAR["is_anonymous"]){?>
<noscript class="sv_nojs"><?php echo $noscript;?></noscript>
<?php }?>
</span>