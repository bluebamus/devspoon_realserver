<?php /* Template_ 2.2.8 2016/11/03 01:18:36 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/skin_bs/visit/basic/visit.skin.html 000001732 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="headline"><h6><strong>사이트 통계</strong></h6></div>
<div class="statistics-wrap">
<ul class="list-unstyled statistics-list">
<li><a href="<?php echo G5_BBS_URL?>/current_connect.php">현재접속자 : <b><?php echo $TPL_VAR["connect"]["total_cnt"]?><?php if($TPL_VAR["connect"]["mb_cnt"]){?> (<span class='color-red'>Member <?php echo $TPL_VAR["connect"]["mb_cnt"]?></span>)<?php }?></b></a></li>
<li>오늘방문자 : <b><?php echo $TPL_VAR["counter"]["visit_today"]?></b></li>
<li>어제방문자 : <b><?php echo $TPL_VAR["counter"]["visit_yesterday"]?></b></li>
<li>최대방문자 : <b><?php echo $TPL_VAR["counter"]["visit_max"]?></b></li>
<li>전체방문자 : <b><?php echo $TPL_VAR["counter"]["visit_total"]?></b></li>
<?php if( 0){?>
<li>신규회원수 : <b><?php echo $TPL_VAR["counter"]["newby"]?></b></li>
<?php }?>
<li>전체회원수 : <b><?php echo $TPL_VAR["counter"]["members"]?></b></li>
<li>전체게시물 : <b><?php echo $TPL_VAR["counter"]["write"]?></b></li>
<?php if( 0){?>
<li>전체코멘트 : <b><?php echo $TPL_VAR["counter"]["comment"]?></b></li>
<?php }?>
</ul>
</div>
<style>
.statistics-wrap {position:relative;overflow:hidden;border:1px solid #e5e5e5;padding:10px}
.statistics-wrap ul {margin-bottom:0}
.statistics-list li:first-child {border-top:none !important}
.statistics-list li {color:#555;font-size:11px;padding:6px 0;display:block;border-top:solid 1px #e8e8e8}
.statistics-list li a {color:#555;font-size:11px;display:block}
.statistics-list li b {color:#555;float:right}
</style>