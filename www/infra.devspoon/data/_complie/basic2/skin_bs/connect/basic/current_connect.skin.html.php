<?php /* Template_ 2.2.8 2017/07/06 22:45:41 /home/tripfooter.com/www/eyoom/theme/basic2/skin_bs/connect/basic/current_connect.skin.html 000002399 */  $this->include_("eb_nameview");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="table-list-eb margin-bottom-20">
<div class="board-list-body">
<table class="table table-hover">
<thead>
<tr>
<th class="th-num">번호</th>
<th>이름</th>
<th class="hidden-xs">위치</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<tr>
<td class="text-left"><?php echo $TPL_V1["num"]?></td>
<td><?php if($TPL_V1["mb_id"]){?><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["mb_nick"],$TPL_V1["mb_email"],$TPL_V1["mb_homepage"])?><?php }else{?><?php echo $TPL_V1["name"]?><?php }?></td>
<td class="td-width hidden-xs">
<div class="td-location">
<?php if($TPL_V1["lo_url"]&&$GLOBALS["is_admin"]=='super'){?>
<a href="<?php echo $TPL_V1["lo_url"]?>"><?php echo $TPL_V1["lo_location"]?></a>
<?php }else{?>
<?php echo $TPL_V1["lo_location"]?>
<?php }?>
</div>
</td>
</tr>
<tr class="td-mobile visible-xs">			        <td colspan="2">
<?php if($TPL_V1["lo_url"]&&$GLOBALS["is_admin"]=='super'){?>
<a href="<?php echo $TPL_V1["lo_url"]?>"><?php echo $TPL_V1["lo_location"]?></a>
<?php }else{?>
<?php echo $TPL_V1["lo_location"]?>
<?php }?>
</td>
</tr>
<?php }}else{?>
<tr><td colspan="3" class="text-center">현재 접속자가 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<style>
.table-list-eb .table thead > tr > th {border-bottom:1px solid #000}
.table-list-eb .table tbody > tr > td {padding:8px 5px}
.table-list-eb .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {background:#fafafa}
.table-list-eb thead {border-top:1px solid #000;border-bottom:1px solid #000;background:#fff}
.table-list-eb th {color:#000;font-weight:bold;white-space:nowrap}
.table-list-eb .th-num {text-align:left !important;padding:8px 5px}
.table-list-eb .td-location {width:300px}
@media (max-width: 1199px) {
.table-list-eb .td-location {width:260px}
}
@media (max-width: 767px) {
.table-list-eb .td-width {width:inherit}
.table-list-eb .td-location {width:280px}
}
.table-list-eb .td-mobile td {border-top:1px solid #f0f0f0;padding:4px 5px !important;font-size:10px;color:#999;background:#fafafa}
</style>