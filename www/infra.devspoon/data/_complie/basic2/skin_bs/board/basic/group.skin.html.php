<?php /* Template_ 2.2.8 2019/12/30 16:10:30 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/board/basic/group.skin.html 000002570 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="basic-group-skin">
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;
$TPL_data_2=empty($TPL_V1["data"])||!is_array($TPL_V1["data"])?0:count($TPL_V1["data"]);?>
<?php if($TPL_I1% 2== 0){?>
<div class="row margin-bottom-20">
<?php }?>
<div class="col-sm-6 md-margin-bottom-20">
<div class="headline">
<h6><strong><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $TPL_V1["bo_table"]?>"><?php echo $TPL_V1["bo_subject"]?></a></strong></h6>
</div>
<div class="group-latest">
<ul class="list-unstyled">
<?php if($TPL_data_2){foreach($TPL_V1["data"] as $TPL_V2){?>
<li>
<a href="<?php echo $TPL_V2["href"]?>">
<div class="group-subj">
- <?php if($TPL_V2["wr_comment"]){?><span class="group-comment">+<?php echo number_format($TPL_V2["wr_comment"])?></span> <?php }?><?php echo $TPL_V2["wr_subject"]?>
</div>
<div class="group-time">
<i class="fa fa-clock-o <?php if($TPL_V2["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('m-d',$TPL_V2["datetime"])?>
</div>
</a>
</li>
<?php }}else{?>
<p class="text-center font-size-12 margin-top-20">최신글이 없습니다.</p>
<?php }?>
</ul>
</div>
</div>
<?php if($TPL_I1% 2== 1||$TPL_I1+ 1==$TPL_list_1){?>
</div>
<div class="clearfix"></div>
<?php }?>
<?php }}?>
</div>
<style>
.group-latest {position:relative;overflow:hidden;border:1px solid #e5e5e5;padding:15px;min-height:193px}
.group-latest ul {margin-bottom:0}
.group-latest li {position:relative;overflow:hidden;padding:2px 0;font-size:12px}
.group-latest .group-subj {position:relative;width:70%;padding-right:0;padding-left:0;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:left}
.group-latest .group-comment {display: inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#74747a}
.group-latest .group-time {position:relative;overflow:hidden;width:30%;font-size:12px;text-align:right;color:#555;float:right}
.group-latest .group-time .i-color {color:#bbb}
.group-latest a:hover .group-subj {text-decoration:underline}
.group-latest a:hover .group-time i {color:#ff2a00}
</style>