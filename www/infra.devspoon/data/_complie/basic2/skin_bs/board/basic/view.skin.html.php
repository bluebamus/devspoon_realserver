<?php /* Template_ 2.2.8 2020/03/31 21:22:39 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/board/basic/view.skin.html 000034783 */  $this->include_("eb_nameview");
$TPL_view_file_1=empty($TPL_VAR["view_file"])||!is_array($TPL_VAR["view_file"])?0:count($TPL_VAR["view_file"]);
$TPL_view_link_1=empty($TPL_VAR["view_link"])||!is_array($TPL_VAR["view_link"])?0:count($TPL_VAR["view_link"]);
$TPL_view_tags_1=empty($TPL_VAR["view_tags"])||!is_array($TPL_VAR["view_tags"])?0:count($TPL_VAR["view_tags"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<article class="board-view">
<h4>
<?php if($GLOBALS["category_name"]){?><span class="color-grey margin-right-5">[<?php echo $TPL_VAR["view"]["ca_name"]?>]</span><?php }?>
<strong><?php echo cut_str(get_text($TPL_VAR["view"]["wr_subject"]), 70)?></strong>
</h4>
<section class="board-view-info">
<div class="pull-left">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]){?>
<span class="view-photo"><?php if($TPL_VAR["view"]["mb_photo"]){?><?php echo $TPL_VAR["view"]["mb_photo"]?><?php }else{?><span class="view-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
<?php }else{?>
<span class="view-photo"><span class="view-user-icon"><i class="fa fa-user"></i></span></span>
<?php }?>
<?php if($TPL_VAR["lv"]["gnu_icon"]){?>
<span style="display:inline-block;margin-right:2px"><img src="<?php echo $TPL_VAR["lv"]["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_VAR["lv"]["eyoom_icon"]){?>
<span style="display:inline-block;margin-right:2px"><img src="<?php echo $TPL_VAR["lv"]["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
<span class="view-nick"><?php echo eb_nameview('basic',$TPL_VAR["view"]["mb_id"],$TPL_VAR["view"]["wr_name"],$TPL_VAR["view"]["wr_email"],$TPL_VAR["view"]["wr_homepage"])?><small><?php if($GLOBALS["is_ip_view"]){?>&nbsp;<?php echo $GLOBALS["ip"]?><?php }?></small></span>
<span><i class="fa fa-comments-o color-grey"></i> <?php echo number_format($TPL_VAR["view"]["wr_comment"])?></span>
</div>
<div class="pull-right text-right">
<span><i class="fa fa-eye color-grey"></i> <?php echo number_format($TPL_VAR["view"]["wr_hit"])?></span>
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
<span><i class="fa fa-clock-o color-grey"></i> <?php echo $TPL_VAR["eb"]->date_time('Y.m.d H:i',$TPL_VAR["view"]["wr_datetime"])?></span>
<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
<span><i class="fa fa-clock-o color-grey"></i> <?php echo $TPL_VAR["eb"]->date_format('Y.m.d H:i',$TPL_VAR["view"]["wr_datetime"])?></span>
<?php }?>
</div>
<div class="clearfix"></div>
</section>
<?php if($GLOBALS["cnt"]> 0){?>
<section class="board-view-file">
<h2>첨부파일</h2>
<ul class="list-unstyled">
<?php if($TPL_view_file_1){foreach($TPL_VAR["view_file"] as $TPL_V1){?>
<li>
<div class="pull-left">
- 첨부파일: <a href="<?php echo $TPL_V1["href"]?>" class="view_file_download"><strong><?php echo $TPL_V1["source"]?></strong> <?php echo $TPL_V1["content"]?> (<?php echo $TPL_V1["size"]?>)</a>
</div>
<div class="pull-right text-right">
<span><i class="fa fa-download color-grey"></i> <?php echo $TPL_V1["download"]?></span>
<span><i class="fa fa-clock-o color-grey"></i> <?php echo $TPL_V1["datetime"]?></span>
</div>
<div class="clearfix"></div>
</li>
<?php }}?>
</ul>
</section>
<?php }?>
<?php if(implode('',$TPL_VAR["view"]["link"])){?>
<section class="board-view-link">
<h2>관련링크</h2>
<ul class="list-unstyled">
<?php if($TPL_view_link_1){foreach($TPL_VAR["view_link"] as $TPL_V1){?>
<li>
<div class="pull-left">
- 관련링크: <a href="<?php echo $TPL_V1["href"]?>" target="_blank"><strong><?php echo $TPL_V1["link"]?></strong></a>
</div>
<div class="pull-right text-right">
<span><?php echo $TPL_V1["hit"]?>회 연결</span>
</div>
<div class="clearfix"></div>
</li>
<?php }}?>
</ul>
</section>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'){?>
<section class="board-view-star">
<h2>별점</h2>
<ul class="list-unstyled star-ratings-view">
<li>- 별점통계: </li>
<li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 0){?>-selected<?php }?> fa fa-star"></i></li>
<li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 1){?>-selected<?php }?> fa fa-star"></i></li>
<li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 2){?>-selected<?php }?> fa fa-star"></i></li>
<li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 3){?>-selected<?php }?> fa fa-star"></i></li>
<li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 4){?>-selected<?php }?> fa fa-star"></i></li>
<li class="margin-left-5">- 평점 : <?php echo $TPL_VAR["rating"]["point"]?>점 (<?php echo number_format($TPL_VAR["rating"]["members"])?>명 참여)</li>
</ul>
<div class="clearfix"></div>
</section>
<?php }?>
<section class="board-view-short-url">
<h2>짧은주소</h2>
<ul class="list-unstyled">
<li>
- 짧은주소:
<a href="<?php echo $TPL_VAR["short_url"]?>" target="_blank"><?php echo $TPL_VAR["short_url"]?></a>
<a href="javascript:void(0);" type="button" data-toggle="modal" data-target=".short-url-modal" class="copy_short_url btn-e btn-e-xs btn-e-light-grey"><span>주소복사</span></a>
</li>
</ul>
</section>
<div class="modal fade short-url-modal" tabindex="-1" role="dialog" aria-labelledby="shorturlhModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
<h4 id="shorturlModalLabel" class="modal-title">짧은 글주소 복사</h4>
</div>
<div class="modal-body">
<form name="short_url_form" class="eyoom-form">
<section>
<label class="input">
<input type="text" name="short_url" id="short_url" value="<?php echo $TPL_VAR["short_url"]?>" onclick="select_text();">
</label>
</section>
<div class="clearfix"></div>
<div class="note"><strong>Note!</strong> 위 주소를 드래그, 복사(Ctrl+C)하여 사용하세요.</div>
</form>
</div>
</div>
</div>
</div>
<div class="view-top-btn">
<?php if($GLOBALS["prev_href"]||$GLOBALS["next_href"]){?>
<ul class="top-btn-left list-unstyled pull-left">
<?php if($GLOBALS["prev_href"]){?><li><a href="<?php echo $GLOBALS["prev_href"]?>" class="btn-e btn-e-light-grey" type="button">이전글</a></li><?php }?>
<?php if($GLOBALS["next_href"]){?><li><a href="<?php echo $GLOBALS["next_href"]?>" class="btn-e btn-e-light-grey" type="button">다음글</a></li><?php }?>
</ul>
<?php }?>
<ul class="top-btn-right list-unstyled pull-right">
<?php if($GLOBALS["copy_href"]){?><li><a href="<?php echo $GLOBALS["copy_href"]?>" class="btn-e btn-e-light-grey" type="button" onclick="board_move(this.href); return false;">복사</a></li><?php }?>
<?php if($GLOBALS["move_href"]){?><li><a href="<?php echo $GLOBALS["move_href"]?>" class="btn-e btn-e-light-grey" type="button" onclick="board_move(this.href); return false;">이동</a></li><?php }?>
<?php if($GLOBALS["search_href"]){?><li><a href="<?php echo $GLOBALS["search_href"]?>" class="btn-e btn-e-dark" type="button">검색</a></li><?php }?>
<?php if($GLOBALS["update_href"]){?><li><a href="<?php echo $GLOBALS["update_href"]?>" class="btn-e btn-e-dark" type="button">수정</a></li><?php }?>
<?php if($GLOBALS["delete_href"]){?><li><a href="<?php echo $GLOBALS["delete_href"]?>" class="btn-e btn-e-dark" type="button" onclick="del(this.href); return false;">삭제</a></li><?php }?>
<li><a href="<?php echo $GLOBALS["list_href"]?>" <?php if($GLOBALS["wmode"]){?>onclick="close_modal();return false;"<?php }?> class="btn-e btn-e-dark" type="button">목록</a></li>
<?php if($GLOBALS["reply_href"]){?><li><a href="<?php echo $GLOBALS["reply_href"]?>" class="btn-e btn-e-dark" type="button">답변</a></li><?php }?>
<?php if($GLOBALS["write_href"]){?><li><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red" type="button">글쓰기</a></li><?php }?>
</ul>
<div class="clearfix"></div>
</div>
<section class="board-view-atc">
<h2 class="board-view-atc-title">본문</h2>
<?php echo $GLOBALS["file_conts"]?>
<?php if($TPL_VAR["ycard"]["yc_blind"]=='y'){?>
<p class='text-center'>-- 블라인드 처리된 글입니다. --</p>
<?php }?>
<div class="board-view-con view-content"><?php echo $GLOBALS["view_content"]?></div>
<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'&&$TPL_VAR["eyoom_board"]["bo_use_tag"]=='1'&&$TPL_view_tags_1> 0){?>
<div class="board-view-tag">
<i class="fa fa-tags"></i>
<?php if($TPL_view_tags_1){foreach($TPL_VAR["view_tags"] as $TPL_V1){?>
<span><a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["tag"]?></a></span>
<?php }}?>
<div class="clearfix"></div>
</div>
<?php }?>
<?php if($GLOBALS["good_href"]||$GLOBALS["nogood_href"]=='1'){?>
<div class="board-view-good-btn">
<?php if($GLOBALS["good_href"]){?>
<span class="board-view-act-gng">
<a href="<?php echo $GLOBALS["good_href"]?>&amp;<?php echo $GLOBALS["qstr"]?>" id="good_button" class="act-gng-btn" type="button"><i class="fa fa-thumbs-up display-block"></i><strong class="display-block"><?php echo number_format($TPL_VAR["view"]["wr_good"])?></strong><div class="mask"><h5>좋아요!</h5></div></a>
<b class="board-view-act-good"></b>
</span>
<?php }?>
<?php if($GLOBALS["nogood_href"]){?>
<span class="board-view-act-gng">
<a href="<?php echo $GLOBALS["nogood_href"]?>&amp;<?php echo $GLOBALS["qstr"]?>" id="nogood_button" class="act-gng-btn" type="button"><i class="fa fa-thumbs-down display-block"></i><strong class="display-block"><?php echo number_format($TPL_VAR["view"]["wr_nogood"])?></strong><div class="mask"><h5>글쎄요~</h5></div></a>
<b class="board-view-act-nogood"></b>
</span>
<?php }?>
</div>
<?php }else{?>
<?php if($TPL_VAR["board"]["bo_use_good"]||$TPL_VAR["board"]["bo_use_nogood"]){?>
<div class="board-view-good-btn">
<?php if($TPL_VAR["board"]["bo_use_good"]){?>
<span class="board-view-act-gng">
<span class="act-gng-btn"><i class="fa fa-thumbs-up display-block"></i><strong class="display-block"><?php echo number_format($TPL_VAR["view"]["wr_good"])?></strong><div class="mask"><h5>좋아요!</h5></div></span>
</span>
<?php }?>
<?php if($TPL_VAR["board"]["bo_use_nogood"]){?>
<span class="board-view-act-gng">
<span class="act-gng-btn"><i class="fa fa-thumbs-down display-block"></i><strong class="display-block"><?php echo number_format($TPL_VAR["view"]["wr_nogood"])?></strong><div class="mask"><h5>좋아요!</h5></div></span>
</span>
<?php }?>
</div>
<?php }?>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$GLOBALS["is_member"]){?>
<form class="eyoom-form">
<div class="eb-rating clearfix">
<div class="rating">
<input type="radio" name="quality" id="quality-5" value="5">
<label for="quality-5"><i class="fa fa-star"></i></label>
<input type="radio" name="quality" id="quality-4" value="4">
<label for="quality-4"><i class="fa fa-star"></i></label>
<input type="radio" name="quality" id="quality-3" value="3">
<label for="quality-3"><i class="fa fa-star"></i></label>
<input type="radio" name="quality" id="quality-2" value="2">
<label for="quality-2"><i class="fa fa-star"></i></label>
<input type="radio" name="quality" id="quality-1" value="1">
<label for="quality-1"><i class="fa fa-star"></i></label>
<strong>별점평가하기</strong>
</div>
</div>
</form>
<?php }?>
<?php if($GLOBALS["is_signature"]&&$TPL_VAR["view"]["mb_id"]!='anonymous'){?>
<?php $this->print_("signature_bs",$TPL_SCP,1);?>
<?php }?>
<?php if($GLOBALS["scrap_href"]||$TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
<div class="board-view-btn">
<?php if($GLOBALS["scrap_href"]){?><a href="<?php echo $GLOBALS["scrap_href"]?>" target="_blank" class="btn btn-default" type="button" onclick="win_scrap(this.href); return false;">스크랩</a><?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'&&$GLOBALS["is_member"]){?>
<?php if(!$TPL_VAR["mb_ycard"]["mb_id"]){?>
<button id="yellow_card" class="btn btn-default" data-toggle="modal" data-target=".yellowcard-modal">신고 <span><?php echo number_format($TPL_VAR["ycard"]["yc_count"])?></span></button>
<?php }else{?>
<button id="cancel_yellow_card" class="btn-e btn-e-red">신고 취소 <span><?php echo number_format($TPL_VAR["ycard"]["yc_count"])?></span></button>
<?php }?>
<?php if($GLOBALS["blind_direct"]){?>
<?php if($TPL_VAR["ycard"]["yc_blind"]!='y'){?>
<button id="direct_blind" class="btn btn-default btn-blind">블라인드</button>
<?php }elseif($TPL_VAR["ycard"]["yc_blind"]=='y'){?>
<button id="cancel_blind" class="btn-e btn-e-red btn-blind">블라인드 취소</button>
<?php }?>
<?php }?>
<?php }?>
</div>
<?php }?>
</section>
<?php if($TPL_VAR["board"]["bo_use_sns"]){?>
<?php $this->print_("sns_bs",$TPL_SCP,1);?>
<?php }?>
<div class="margin-hr-15"></div>
<?php $this->print_("cmt_bs",$TPL_SCP,1);?>
<div class="board-view-bot">
<?php echo $GLOBALS["link_buttons"]?>
</div>
</article>
<div class="margin-bottom-40"></div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
<div class="modal fade yellowcard-modal" tabindex="-1" role="dialog" aria-labelledby="yellowcardModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
<h6 id="myLargeModalLabel" class="modal-title"><strong>게시물 신고하기</strong></h6>
</div>
<div class="modal-body">
<fieldset id="bo_ycard" class="eyoom-form">
<form name="fycard">
<input type="hidden" name="modal_cmt_id" id="modal_cmt_id" value="">
<label for="sfl" class="sound_only">신고사유</label>
<label class="label">이 게시물을 신고하시겠습니까? 신고사유를 선택해 주세요.</label>
<section class="bg-light">
<div class="inline-group">
<label class="radio" for="ycard_reason_1">
<input type="radio" name="ycard_reason" id="ycard_reason_1" value="1"><i class="rounded-x"></i>광고성
</label>
<label class="radio" for="ycard_reason_2">
<input type="radio" name="ycard_reason" id="ycard_reason_2" value="2"><i class="rounded-x"></i>음란성
</label>
<label class="radio" for="ycard_reason_3">
<input type="radio" name="ycard_reason" id="ycard_reason_3" value="3"><i class="rounded-x"></i>비방성
</label>
<label class="radio" for="ycard_reason_4">
<input type="radio" name="ycard_reason" id="ycard_reason_4" value="4"><i class="rounded-x"></i>혐오성
</label>
<label class="radio" for="ycard_reason_5">
<input type="radio" name="ycard_reason" id="ycard_reason_5" value="5"><i class="rounded-x"></i>기타
</label>
</div>
</section>
<div class="text-center margin-top-20 margin-bottom-10">
<button type="button" class="btn-e btn-e-red">신고하기</button>
</div>
</form>
</fieldset>
</div>
<div class="modal-footer">
<button data-dismiss="modal" class="btn-e btn-e-dark" type="button">닫기</button>
</div>
</div>
</div>
</div>
<?php }?>
<style>
.board-view .margin-hr-15 {height:1px;border-top:1px solid #ccc;margin:15px 0;clear:both}
.board-view .board-view-info {border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding:7px 0;margin-top:15px;font-size:11px}
.board-view .board-view-info h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-info .pull-left span {margin-right:5px}
.board-view .board-view-info .pull-right {line-height:26px}
.board-view .board-view-info .pull-right span {margin-left:5px}
.board-view .view-member {position:relative;overflow:hidden;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;width:15%;float:left}
.board-view .view-photo img {width:26px;height:26px;margin-right:2px}
.board-view .view-photo .view-user-icon {width:26px;height:26px;font-size:16px;line-height:26px;text-align:center;background:#84848a;color:#fff;margin-right:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.board-view .view-nick {font-size:12px}
.board-view .board-view-file {font-size:11px}
.board-view .board-view-file h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-file ul {margin-bottom:0}
.board-view .board-view-file li {padding:6px 0;border-bottom:1px solid #eee}
.board-view .board-view-file span {margin-left:5px}
.board-view .board-view-link {font-size:11px}
.board-view .board-view-link h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-link ul {margin-bottom:0}
.board-view .board-view-link li {padding:6px 0;border-bottom:1px solid #eee}
.board-view .board-view-short-url {font-size:11px}
.board-view .board-view-short-url h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-short-url ul {margin-bottom:0}
.board-view .board-view-short-url li {padding:6px 0;border-bottom:1px solid #ccc}
.board-view .board-view-star {padding:6px 0;border-bottom:1px solid #eee;font-size:11px}
.board-view .board-view-star h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-star ul {margin-bottom:0}
.board-view .board-view-star .star-ratings-view li {padding:0;float:left;margin-right:2px}
.board-view .board-view-star .star-ratings-view li .rating {color:#ccc;font-size:11px;line-height:normal}
.board-view .board-view-star .star-ratings-view li .rating-selected {color:#e33334;font-size:11px}
.board-view .view-top-btn {margin:0 0 10px;padding:10px 0;zoom:1}
.board-view .view-top-btn:after {display:block;visibility:hidden;clear:both;content:""}
.board-view .view-top-btn h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .view-top-btn .top-btn-left li {float:left;margin-right:5px}
.board-view .view-top-btn .top-btn-right li {float:left;margin-left:5px;margin-bottom:5px}
.board-view .board-view-atc {min-height:200px;height:auto !important;height:200px}
.board-view .board-view-atc-title {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-img {margin:0;width:100%;overflow:hidden;zoom:1}
.board-view .board-view-img:after {display:block;visibility:hidden;clear:both;content:""}
.board-view .board-view-img img {max-width:100%;height:auto}
.board-view .board-view-con {margin-bottom:30px;width:100%;line-height:1.7;word-break:break-all;overflow:hidden}
.board-view .board-view-con a {color:#000;text-decoration:underline}
.board-view .board-view-con img {max-width:100%;height:auto}
.board-view .board-view-good-btn {margin-bottom:30px;text-align:center}
.board-view .board-view-good-btn .board-view-act-gng {position:relative;margin:0 5px}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn {position:relative;overflow:hidden;width:70px;height:70px;border:1px solid #888;display:inline-block;white-space:nowrap;vertical-align:baseline;text-align:center}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn i {font-size:34px;color:#ddd;margin:5px 0 3px}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover i {color:#ff2a00}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn strong {font-size:13px;color:#000}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn .mask {width:100%;height:100%;position:absolute;overflow:hidden;top:0;left:0;background:#fff}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn .mask {-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn h5 {color:#000;font-size:13px;font-weight:bold;text-align:center;margin-top:40px;background:transparent;-webkit-transform:scale(0);-moz-transform:scale(0);-o-transform:scale(0);-ms-transform:scale(0);transform:scale(0);-webkit-transition:all 0.2s linear;-moz-transition:all 0.2s linear;-o-transition:all 0.2s linear;-ms-transition:all 0.2s linear;transition:all 0.2s linear;-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover .mask {-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=90)";filter:alpha(opacity=90);opacity:0.9}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover h5 {-webkit-transform:scale(1);-moz-transform:scale(1);-o-transform:scale(1);-ms-transform:scale(1);transform:scale(1);-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";filter:alpha(opacity=100);opacity:1}
.board-view .board-view-btn {margin-bottom:30px;text-align:right}
.board-view .board-view-btn a {margin-right:5px;vertical-align:middle}
.board-view .board-view-btn strong {color:#ff2a00}
.board-view .board-view-act-good,.board-view-act-nogood {display:none;position:absolute;top:30px;left:0;padding:5px 0;width:165px;background:#000;color:#fff;text-align:center}
.board-view .board-view-bot {zoom:1}
.board-view .board-view-bot:after {display:block;visibility:hidden;clear:both;content:""}
.board-view .board-view-bot h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-bot ul {margin:0;padding:0;list-style:none}
.board-view .blind {display:none}
.board-view .map-content-wrap {width:100%;height:350px}
.board-view .map-content-wrap > div {width:100%;height:350px}
/* 게시판 보기에서 이미지 자동조절 */
.board-view .board-view-con .photo-image,.board-view-con .photo-image {margin:0 10px}
.board-view .board-view-con .photo-image img,.board-view-con .photo-image img {width:100%}
.board-view .board-view-con .photo-text,.board-view-con .photo-text {color:#777;font-size:11px;line-height:13pt;margin:0;padding:5px;border:1px solid #ddd;border-top:0}
.board-view .board-view-con a{text-decoration:none}
/* 별점평가 */
.board-view .eb-rating {width:170px;margin:0 auto 30px;border:1px solid #888;padding:3px 10px}
.board-view .eyoom-form .rating {font-size:12px}
.board-view .eyoom-form .rating label {font-size:14px}
.board-view .eyoom-form .rating input:checked ~ label {color:#e33334}
.board-view .eyoom-form .rating input + label:hover,
.board-view .eyoom-form .rating input + label:hover ~ label {color:#e33334}
/* 태그 */
.board-view-tag {position:relative;overflow:hidden;border:1px solid #eee;padding:5px;margin:20px 0}
.board-view-tag span {display:inline-block;padding:0px 5px;margin:2px;background:#eee;font-size:11px;}
.board-view-tag .fa-tags {width:22px;height:22px;line-height:22px;text-align:center;font-size:12px;background:#54545a;color:#fff;margin-right:5px}
</style>
<script src="/js/viewimageresize.js"></script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="http://openapi.map.naver.com/openapi/v2/maps.js?clientId=네이버지도_clientId"></script>
<script src="http://apis.daum.net/maps/maps3.js?apikey=다음_api_key"></script>
<script src="/eyoom/theme/basic2/js/eyoom.map.js"></script>
<script>
$(function(){
$(".map-content-wrap").each(function(){
var id      = $(this).find('div').attr('id');
var type    = $(this).attr('data-map-type');
var name    = $(this).attr('data-map-name');
var x       = $(this).attr('data-map-x');
var y       = $(this).attr('data-map-y');
var address = $(this).attr('data-map-address');
switch(type) {
case 'google':
loading_google_map(id, x, y, name, address);
break;
case 'naver':
loading_naver_map(id, x, y, name, address);
break;
case 'daum':
loading_daum_map(id, x, y, name, address);
break;
}
});
});
</script>
<?php }?>
<script>
<?php if($TPL_VAR["board"]["bo_download_point"]< 0){?>
$(function() {
$("a.view_file_download").click(function() {
if(!g5_is_member) {
alert("다운로드 권한이 없습니다.\n회원이시라면 로그인 후 이용해 보십시오.");
return false;
}
var msg = "파일을 다운로드 하시면 포인트가 차감(<?php echo number_format($TPL_VAR["board"]["bo_download_point"])?>점)됩니다.\n\n포인트는 게시물당 한번만 차감되며 다음에 다시 다운로드 하셔도 중복하여 차감하지 않습니다.\n\n그래도 다운로드 하시겠습니까?";
if(confirm(msg)) {
var href = $(this).attr("href")+"&js=on";
$(this).attr("href", href);
return true;
} else {
return false;
}
});
});
<?php }?>
function close_modal(wr_id) {
window.parent.closeModal(wr_id);
}
function board_move(href)
{
window.open(href, "boardmove", "left=50, top=50, width=500, height=550, scrollbars=1");
}
</script>
<script>
$(function() {
$("a.view_image").click(function() {
window.open(this.href, "large_image", "location=yes,links=no,toolbar=no,top=10,left=10,width=10,height=10,resizable=yes,scrollbars=no,status=no");
return false;
});
// 추천, 비추천
$("#good_button, #nogood_button").click(function() {
var $tx;
if(this.id == "good_button")
$tx = $(".board-view-act-good");
else
$tx = $(".board-view-act-nogood");
excute_good(this.href, $(this), $tx);
return false;
});
// 이미지 리사이즈
$(".board-view-atc").viewimageresize();
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'&&$GLOBALS["is_member"]){?>
// 게시물 신고
$('.yellowcard-modal .modal-body button').click(function () {
var cmt_id = $("#modal_cmt_id").val();
var yc_reason = $(':radio[name="ycard_reason"]:checked').val();
if(!yc_reason) {
alert('신고사유를 선택해 주세요.');
return;
} else {
$.post(
'<?php echo EYOOM_CORE_URL?>/board/yellow_card.php',
{ bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: "add", reason: yc_reason },
function(data) {
if(data.count && !data.error) {
if(!cmt_id) {
if($('#yc_card').text() == '') {
$('#yellow_card').wrap('<span id="yc_card"></span>');
} else {
$('#yc_card').html('');
}
$('#yellow_card').remove();
var yc_html = '<button id="cancel_yellow_card" class="btn-e btn-e-dark">신고 취소 <span>' + number_format(String(data.count)) + '</span></button>';
$('#yc_card').html(yc_html);
} else {
var obj = $("#cmt_yellow_card_li_"+cmt_id);
obj.html('');
obj.attr('data-original-title','신고취소');
$("#modal_cmt_id").val('');
var yc_html = '<button id="cancel_cmt_yellow_card_'+cmt_id+'" class="btn-e btn-e-dark btn-e-xs cancel_cmt_yellow_card" data-cmt-id="'+cmt_id+'"><i class="fa fa-trash"></i> <span>' + number_format(String(data.count)) + '</span></button>';
obj.html(yc_html);
}
}
if(data.msg) alert(data.msg);
$('.yellowcard-modal').modal('hide');
}, "json"
);
}
});
// 게시물 신고 취소
$('#cancel_yellow_card, .cancel_yellow_card, .cancel_cmt_yellow_card').click(function() {
var cmt_id = $("#modal_cmt_id").val();
if(confirm('신고취소 처리하시겠습니까?')) {
$.post(
'<?php echo EYOOM_CORE_URL?>/board/yellow_card.php',
{ bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: "cancel" },
function(data) {
if(data.count>=0 && !data.error) {
if(!cmt_id) {
if($('#yc_card').text() == '') {
$('#cancel_yellow_card').wrap('<span id="yc_card"></span>');
} else {
$('#yc_card').html('');
}
$('#cancel_yellow_card').remove();
var yc_html = '<button id="yellow_card" class="btn-e btn-e-yellow" data-toggle="modal" data-target=".yellowcard-modal">신고 <span>' + number_format(String(data.count)) + '</span></button>';
} else {
var obj = $("#cancel_cmt_yellow_card_li_"+cmt_id);
obj.html('');
obj.attr('data-original-title','신고');
$("#modal_cmt_id").val('');
var yc_html = '<button id="cmt_yellow_card_'+cmt_id+'" class="btn-e btn-e-red btn-e-xs cmt_yellow_card" data-toggle="modal" data-target=".yellowcard-modal" data-cmt-id="'+cmt_id+'"><i class="fa fa-trash"></i> <span>' + number_format(String(data.count)) + '</span></button>';
obj.html(yc_html);
}
$('#yc_card').html(yc_html);
}
if(data.msg) alert(data.msg);
}, "json"
);
}
});
// 원글의 신고취소를 위해 modal_cmt_id 값을 리셋
$('#yellow_card').click(function() {
$("#modal_cmt_id").val('');
});
<?php if($GLOBALS["blind_direct"]){?>
// 블라인드 처리 권한을 가진 회원
$('.btn-blind, .btn-cmt-blind').click(function() {
var id = $(this).attr('id');
var cmt_id = $(this).attr('data-cmt-id');
if(typeof(cmt_id) == 'undefined') var cmt_id = '';
switch(id) {
case 'direct_blind':
if(confirm('본 게시물을 바로 블라인드 처리합니다.\n\n계속 진행하시겠습니까?'))    {
var action = 'db'; // direct blind
var re_id = !cmt_id ? 'cancel_blind' : 'cancel_cmt_blind_li_'+cmt_id;
var re_class = !cmt_id ? 'btn-e btn-e-red btn-blind' : 'btn-e btn-e-dark btn-e-xs btn-blind';
var re_text = '블라인드 취소';
}
break;
case 'cancel_blind':
if(confirm('본 게시물을 블라인드 취소처리합니다.\n\n계속 진행하시겠습니까?'))     {
var action = 'cb'; // cancel blind
var re_id = !cmt_id ? 'direct_blind' : 'direct_cmt_blind_li_'+cmt_id;
var re_class = !cmt_id ? 'btn btn-default btn-blind' : 'btn-e btn-e-red btn-e-xs btn-blind';
var re_text = '블라인드';
}
break;
case 'direct_cmt_blind_li_'+cmt_id:
if(confirm('본 댓글을 바로 블라인드 처리합니다.\n\n계속 진행하시겠습니까?'))     {
var action = 'db'; // direct blind
var re_id = 'cancel_cmt_blind_li_'+cmt_id;
var re_class = 'btn-e btn-e-dark btn-e-xs btn-cmt-blind';
}
break;
case 'cancel_cmt_blind_li_'+cmt_id:
if(confirm('본 댓글을 블라인드 취소처리합니다.\n\n계속 진행하시겠습니까?'))  {
var action = 'cb'; // cancel blind
var re_id = 'direct_cmt_blind_li_'+cmt_id;
var re_class = 'btn-e btn-e-red btn-e-xs btn-cmt-blind';
}
break;
}
if(typeof(action) != 'undefined') {
$.post(
'<?php echo EYOOM_CORE_URL?>/board/direct_blind.php',
{ bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: action },
function(data) {
if(!cmt_id) {
$('.btn-blind').attr('id', re_id);
$('.btn-blind').attr('class', re_class);
$('.btn-blind').text(re_text);
} else {
$('.btn-cmt-blind').attr('id', re_id);
$('.btn-cmt-blind').attr('class', re_class);
}
if(data.msg) alert(data.msg);
}, "json"
);
}
});
<?php }?>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$GLOBALS["is_member"]){?>
$(".rating > input").click(function() {
var score = $(this).val();
if(score && confirm("별점 " + score + " 점을 클릭하였습니다.\n\n본 게시물의 별점평가에 참여하시겠습니까?") ){
$.post(
'<?php echo EYOOM_CORE_URL?>/board/star_rating.php',
{ bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", score: score },
function(data) {
if(data.msg) alert(data.msg);
}, "json"
);
}
});
<?php }?>
});
function excute_good(href, $el, $tx)
{
$.post(
href,
{ js: "on" },
function(data) {
if(data.error) {
alert(data.error);
return false;
}
if(data.count) {
$el.find("strong").text(number_format(String(data.count)));
if($tx.attr("id").search("nogood") > -1) {
$tx.text("이 글을 비추천하셨습니다.");
$tx.fadeIn(200).delay(2500).fadeOut(200);
} else {
$tx.text("이 글을 추천하셨습니다.");
$tx.fadeIn(200).delay(2500).fadeOut(200);
}
}
}, "json"
);
}
</script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_coding"]=='1'){?>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shCore.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shLegacy.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushAppleScript.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushAS3.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushBash.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushColdFusion.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushCpp.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushCSharp.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushDelphi.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushDiff.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushErlang.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushGroovy.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushJava.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushPerl.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushPhp.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushPlain.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushPowerShell.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushPython.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushRuby.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushSass.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushScala.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushVb.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/syntaxhighlighter/scripts/shBrushXml.js"></script>
<link type="text/css" rel="stylesheet" href="/eyoom/theme/basic2/plugins/syntaxhighlighter/styles/shCoreDefault.css"/>
<script type="text/javascript">SyntaxHighlighter.all();</script>
<?php }?>