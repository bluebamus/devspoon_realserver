<?php /* Template_ 2.2.8 2020/03/31 21:22:39 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/board/basic/view_comment.skin.html 000031911 */  $this->include_("eb_nameview");
$TPL_cmt_list_1=empty($TPL_VAR["cmt_list"])||!is_array($TPL_VAR["cmt_list"])?0:count($TPL_VAR["cmt_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/venobox/venobox.css" type="text/css" media="screen">',0);
?>
<style>
.comment-area {font-size:12px}
.comment-area .margin-hr-15 {height:1px;border-top:1px solid #ccc;margin:15px 0;clear:both}
.view-comment {position:relative;font-size:12px;position:relative}
.view-comment h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.view-comment-item {position:relative;padding:10px 0;border-top:1px solid #e5e5e5}
.view-comment-item .no-comment {text-align:center;padding:10px 0 20px;border-bottom:1px solid #e5e5e5}
.view-comment-no-item {position:relative;padding:10px 0;border-top:1px solid #e5e5e5}
.view-comment-no-item .no-comment {text-align:center;padding:10px 0 20px;border-bottom:1px solid #e5e5e5}
.view-comment-more {text-align:center;border-top:1px solid #e5e5e5;padding-top:15px}
.view-comment-item.cmt-best {background:#fffdf0;border-color:rgba(226, 131, 39, 0.3)}
.view-comment .view-comment-photo {float:left;margin-right:10px}
.view-comment .view-comment-photo .comment-user-icon {width:50px;height:50px;background:#84848a;font-size:30px;line-height:50px;text-align:center;color:#fff;margin-bottom:10px;display:inline-block}
.view-comment .view-comment-photo img {width:50px;height:50px;margin-bottom:10px}
.view-comment .comment-item-body {padding-left:65px}
@media (max-width: 767px) {
.view-comment .view-comment-photo .comment-user-icon {width:30px;height:30px;font-size:20px;line-height:30px}
.view-comment .view-comment-photo img {width:30px;height:30px}
.view-comment .comment-item-body {padding-left:40px}
}
.view-comment .comment-item-body-pn {padding-left:0}
.view-comment .comment-item-info {position:relative;margin-bottom:5px}
.view-comment .comment-item-info span {margin-right:3px}
.view-comment .comment-ip {font-size:11px;color:#aaa}
.view-comment .comment-time {float:right;font-size:11px}
.view-comment .comment-btn .list-inline > li {padding-left:1px;padding-right:1px}
.comment-area .comment-write-wrap {position:relative; border-top:1px solid #e5e5e5;margin-top:10px;padding:10px 0 0}
.comment-area .comment-write-submit {position:relative;text-align:right}
.comment-area .comment-function-box {margin-top:10px;border:1px solid #e5e5e5;padding:15px 10px}
#bo_vc_send_sns ul{padding:0;margin:-5px 0 20px;overflow:hidden}
#bo_vc_send_sns li{float:left;margin-right:10px;list-style:none}
#bo_vc_send_sns li input{margin:10px 25px 10px 5px}
#infscr-loading {text-align:center;z-index:100;position:absolute;left:50%;bottom:0;width:200px;margin-left:-100px;padding:10px;background:#000;opacity:0.6;color:#fff}
</style>
<script>
// 글자수 제한
var char_min = parseInt(<?php echo $GLOBALS["comment_min"]?>); // 최소
var char_max = parseInt(<?php echo $GLOBALS["comment_max"]?>); // 최대
</script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/venobox/venobox.min.js"></script>
<div class="comment-area">
<div class="view-comment">
<h5><strong>댓글목록</strong></h5>
<?php if($TPL_cmt_list_1){$TPL_I1=-1;foreach($TPL_VAR["cmt_list"] as $TPL_V1){$TPL_I1++;?>
<h2><?php echo $TPL_V1["wr_name"]?>님의 댓글</h2>
<div id="c_<?php echo $TPL_V1["comment_id"]?>" class="view-comment-item <?php if($TPL_V1["is_cmt_best"]){?>cmt-best<?php }?>" style="<?php if($TPL_V1["cmt_depth"]&&!$TPL_V1["is_cmt_best"]){?>margin-left:<?php echo $TPL_V1["cmt_depth"]?>px;<?php }?>">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]){?>
<div class="media-photo">
<span class="view-comment-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="comment-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
</div>
<div class="comment-item-body">
<?php }else{?>
<div class="comment-item-body-pn">
<?php }?>
<div class="comment-item-info">
<?php if($TPL_V1["gnu_icon"]){?>
<span style="display:inline-block;margin-right:2px"><img src="<?php echo $TPL_V1["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_V1["eyoom_icon"]){?>
<span style="display:inline-block;margin-right:2px"><img src="<?php echo $TPL_V1["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
<span class="comment-name"><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["wr_name"],$TPL_V1["wr_email"],$TPL_V1["wr_homepage"])?></span> <?php if($TPL_V1["is_cmt_best"]){?><span class="badge badge-purple">BEST <?php echo $TPL_I1+ 1?></span><?php }?><?php if($GLOBALS["is_ip_view"]){?> <span class="comment-ip"><?php echo $TPL_V1["ip"]?></span><?php }?>
<span class="comment-time">
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
<i class="fa fa-clock-o color-grey"></i> <?php echo $TPL_VAR["eb"]->date_time('Y.m.d H:i',$TPL_V1["datetime"])?>
<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
<i class="fa fa-clock-o color-grey"></i> <?php echo $TPL_VAR["eb"]->date_format('Y.m.d H:i',$TPL_V1["datetime"])?>
<?php }?>
</span>
</div>
<?php if($TPL_V1["yc_blind"]&&$TPL_V1["yc_cannotsee"]){?>
<p class="text-center">-- 이글은 블라인드 처리된 댓글입니다. --</p>
<?php }else{?>
<p>
<?php if($TPL_V1["yc_blind"]){?>
<p class="text-center">-- 이글은 블라인드 처리된 댓글입니다. --</p>
<?php }?>
<?php if(strstr($TPL_V1["wr_option"],'secret')){?><i class="fa fa-lock color-red"></i> <?php }?>
<?php if($TPL_V1["imgsrc"]){?>
<img src="<?php echo $TPL_V1["imgsrc"]?>" class="img-responsive"><br>
<?php }?>
<?php echo $TPL_V1["comment"]?>
</p>
<?php }?>
<div class="comment-btn">
<?php if($TPL_V1["is_reply"]||$TPL_V1["is_edit"]||$TPL_V1["is_del"]||$TPL_V1["c_good_href"]||$TPL_V1["c_nogood_href"]){?>
<ul class="list-inline reply-list pull-right">
<?php if($TPL_V1["is_reply"]){?><li><a href="<?php echo $TPL_V1["c_reply_href"]?>" onclick="comment_box('<?php echo $TPL_V1["comment_id"]?>', 'c'); return false;" class="btn-e btn-e-xs btn-e-red">댓글</a></li><?php }?>
<?php if($TPL_V1["is_edit"]){?><li><a href="<?php echo $TPL_V1["c_edit_href"]?>" onclick="comment_box('<?php echo $TPL_V1["comment_id"]?>', 'cu'); return false;" class="btn-e btn-e-xs btn-e-light-grey">수정</a></li><?php }?>
<?php if($TPL_V1["is_del"]){?><li><a href="<?php echo $TPL_V1["del_link"]?>" onclick="return comment_delete();" class="btn-e btn-e-xs btn-e-light-grey">삭제</a></li><?php }?>
<?php if($TPL_V1["c_good_href"]){?>
<li class="margin-left-5"><a href="<?php echo $TPL_V1["c_good_href"]?>" id="goodcmt_button_<?php echo $TPL_V1["comment_id"]?>" class="goodcmt_button btn-e btn-e-dark btn-e-xs" type="button" title="공감"><i class="fa fa-thumbs-up color-light-grey"></i> <strong class="board-cmt-act-good"><?php if($TPL_V1["good"]){?><span class="color-yellow"><?php echo $TPL_V1["good"]?></span><?php }else{?><span class="color-light-grey">0</span><?php }?></strong></a></li>
<?php }?>
<?php if($TPL_V1["c_nogood_href"]){?>
<li><a href="<?php echo $TPL_V1["c_nogood_href"]?>" id="nogoodcmt_button_<?php echo $TPL_V1["comment_id"]?>" class="nogoodcmt_button btn-e btn-e-dark btn-e-xs" type="button" title="비공감"><i class="fa fa-thumbs-down color-light-grey"></i> <strong class="board-cmt-act-nogood"><?php if($TPL_V1["nogood"]){?><span class="color-red"><?php echo $TPL_V1["nogood"]?></span><?php }else{?><span class="color-light-grey">0</span><?php }?></strong></a></li>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
<?php if(!$TPL_V1["mb_ycard"]["mb_id"]){?>
<li id="cmt_yellow_card_li_<?php echo $TPL_V1["comment_id"]?>" data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="신고"><button id="cmt_yellow_card_<?php echo $TPL_V1["comment_id"]?>" class="btn-e btn-e-light-grey btn-e-xs cmt_yellow_card" data-toggle="modal" data-target=".yellowcard-modal" data-cmt-id="<?php echo $TPL_V1["comment_id"]?>"><i class="fa fa-trash"></i> <span><?php echo number_format($TPL_V1["yc_count"])?></span></button></li>
<?php }else{?>
<li id="cancel_cmt_yellow_card_li_<?php echo $TPL_V1["comment_id"]?>" data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="신고취소"><button id="cancel_cmt_yellow_card_<?php echo $TPL_V1["comment_id"]?>" class="btn-e btn-e-dark btn-e-xs cancel_cmt_yellow_card" data-cmt-id="<?php echo $TPL_V1["comment_id"]?>"><i class="fa fa-trash"></i> <span><?php echo number_format($TPL_V1["yc_count"])?></span></button></li>
<?php }?>
<?php if($GLOBALS["blind_direct"]){?>
<?php if($TPL_V1["yc_blind"]!='y'){?>
<li data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Blind"><button id="direct_cmt_blind_li_<?php echo $TPL_V1["comment_id"]?>" class="btn-e btn-e-light-grey btn-e-xs btn-cmt-blind" data-cmt-id="<?php echo $TPL_V1["comment_id"]?>"><i class="fa fa-eye-slash"></i></button></li>
<?php }elseif($TPL_V1["yc_blind"]=='y'){?>
<li data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Cancel"><button id="cancel_cmt_blind_li_<?php echo $TPL_V1["comment_id"]?>" class="btn-e btn-e-dark btn-e-xs btn-cmt-blind" data-cmt-id="<?php echo $TPL_V1["comment_id"]?>"><i class="fa fa-eye"></i></button></li>
<?php }?>
<?php }?>
<?php }?>
</ul>
<?php }?>
</div>
<div class="clearfix"></div>
<span id="edit_<?php echo $TPL_V1["comment_id"]?>"></span>				<span id="reply_<?php echo $TPL_V1["comment_id"]?>"></span>
<input type="hidden" value="<?php echo strstr($TPL_V1["wr_option"],'secret')?>" id="secret_comment_<?php echo $TPL_V1["comment_id"]?>">
<input type="hidden" value="<?php echo $TPL_V1["anonymous_id"]?>" id="anonymous_id_<?php echo $TPL_V1["comment_id"]?>">
<input type="hidden" value="<?php echo $TPL_V1["imgname"]?>" id="imgname_<?php echo $TPL_V1["comment_id"]?>">
<textarea id="save_comment_<?php echo $TPL_V1["comment_id"]?>" style="display:none"><?php echo $TPL_V1["content1"]?></textarea>
<div class="clearfix"></div>
</div>
</div>
<?php }}else{?>
<div class="view-comment-no-item">
<p id="bo_vc_empty" class="no-comment"><i class="fa fa-exclamation-circle"></i> 등록된 댓글이 없습니다.</p>
</div>
<?php }?>
</div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_cmt_infinite"]=='1'){?>
<div id="infinite_pagination">
<a class="next" href="<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $GLOBALS["bo_table"]?>&wr_id=<?php echo $GLOBALS["wr_id"]?>&sca=<?php echo $GLOBALS["sca"]?>&cpage=<?php echo $TPL_VAR["cpage"]+ 1?>"></a>
</div>
<?php if(count($TPL_VAR["cmt_list"])> 0){?>
<div class="view-comment-more">
<a id="view-comment-more" href="#" class="btn-e btn-e-red btn-e-lg">댓글 더보기</a>
</div>
<?php }?>
<?php }?>
<div class="margin-hr-15"></div>
<?php if($GLOBALS["is_comment_write"]){?>
<div id="view-comment-write">
<form name="fviewcomment" action="./write_comment_update.php" onsubmit="return fviewcomment_submit(this);" method="post" autocomplete="off" class="eyoom-form view-comment-write-box" enctype="multipart/form-data">
<input type="hidden" name="w" value="<?php if(!$GLOBALS["w"]){?>c<?php }else{?><?php echo $GLOBALS["w"]?><?php }?>" id="w">
<input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
<input type="hidden" name="wr_id" value="<?php echo $GLOBALS["wr_id"]?>">
<input type="hidden" name="comment_id" value="<?php echo $GLOBALS["c_id"]?>" id="comment_id">
<input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
<input type="hidden" name="spt" value="<?php echo $GLOBALS["spt"]?>">
<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
<input type="hidden" name="is_good" value="">
<input type="hidden" name="board_skin_path" value="<?php echo EYOOM_CORE_PATH?>/board">
<input type="hidden" name="wr_1" value="<?php echo $GLOBALS["wr_1"]?>">
<input type="hidden" name="cmt_amt" value="<?php echo $GLOBALS["cmt_amt"]?>">
<input type="hidden" name="wmode" value="<?php echo $GLOBALS["wmode"]?>">
<h5><strong>댓글쓰기</strong></h5>
<div class="comment-write-wrap">
<div class="row">
<?php if(!$GLOBALS["is_member"]){?>
<section class="col col-4">
<label for="wr_name" class="label">이름<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-user"></i>
<input type="text" name="wr_name" value="<?php echo get_cookie('ck_sns_name')?>" id="wr_name" required size="5" maxLength="20">
</label>
</section>
<section class="col col-4">
<label for="wr_password" class="label">비밀번호<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-append fa fa-lock"></i>
<input type="password" name="wr_password" id="wr_password" required size="10" maxLength="20">
</label>
</section>
<?php }?>
<section class="col col-4">
<label class="checkbox pull-left"><input type="checkbox" name="wr_secret" value="secret" id="wr_secret"><i></i>비밀글 사용</label>
<?php if($GLOBALS["is_anonymous"]){?>
<label class="checkbox pull-left margin-left-10"><input type="checkbox" name="anonymous" value="y" id="anonymous"><i></i>익명글 사용</label>
<?php }?>
<div class="clearfix"></div>
</section>
</div>
<?php if($TPL_VAR["board"]["bo_use_sns"]&&($TPL_VAR["config"]["cf_facebook_appid"]||$TPL_VAR["config"]["cf_twitter_key"])){?>
<label class="label">SNS 동시등록</label>
<div id="bo_vc_send_sns"></div>
<div class="clear"></div>
<?php }?>
<section>
<div id="comment-option">
<div class="panel panel-default" style="border:0;margin-bottom:0;box-shadow:none">
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_cmtimg"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-default" data-toggle="collapse" data-parent="#comment-option" href="#collapse-image-cm">이미지첨부</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-default" data-toggle="collapse" data-parent="#comment-option" href="#collapse-video-cm">동영상</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-default" data-toggle="collapse" data-parent="#comment-option" href="#collapse-sound-cm">사운드클라우드</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_coding"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-default" data-toggle="collapse" data-parent="#comment-option" href="#collapse-code-cm">코드</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-default" data-toggle="collapse" data-parent="#comment-option" href="#collapse-map-cm">지도</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
<a class="btn-e btn-e-xs btn-e-dark pull-right emoticon" data-type="iframe" title="이모티콘" href="<?php echo EYOOM_CORE_URL?>/board/emoticon.php">이모티콘</a>
<?php }?>
<div class="clearfix"></div>
<div id="collapse-image-cm" class="panel-collapse collapse">
<div class="comment-function-box">
<label for="file" class="input input-file">
<div class="button bg-color-light-grey"><input type="file" id="file" name="cmt_file[]" value="이미지선택" title="파일첨부 : 용량 <?php echo $GLOBALS["upload_max_filesize"]?> 이하만 업로드 가능" onchange="this.parentNode.nextSibling.value = this.value">Image</div><input type="text" readonly>
</label>
<div id="del_cmtimg"></div>
</div>
</div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
<div id="collapse-video-cm" class="panel-collapse collapse">
<div class="comment-function-box">
<label class="input input-file">
<a href="javascript:;" class="button bg-color-light-grey color-white" id="btn_video" onclick="return false;">확인</a>
<input type="text" id="video_url" class="form-control" placeholder="동영상주소">
</label>
</div>
</div>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
<div id="collapse-sound-cm" class="panel-collapse collapse">
<div class="comment-function-box">
<div class="row">
<div class="col col-8 left">
<label class="input input-file">
<a href="javascript:;" class="button bg-color-light-grey color-white" id="btn_scloud" onclick="return false;">확인</a>
<input type="text" id="scloud_url" class="form-control" placeholder="사운드클라우드 음원주소">
</label>
</div>
<div class="col col-4 right text-right">
<a href="https://soundcloud.com/" target="_blank" class="btn-e btn-e-xs btn-e-light-grey"><i class="fa fa-location-arrow"></i> 사운드클라우드 GO</a>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<div id="collapse-map-cm" class="panel-collapse collapse">
<div class="comment-function-box">
<div class="row">
<div class="col col-2 md-margin-bottom-10">
<label class="input">
<i class="icon-append fa fa-question-circle"></i>
<input type="text" name="map_zip" id="map_zip" size="5" maxlength="6">
<b class="tooltip tooltip-top-right">우편번호</b>
</label>
</div>
<div class="col col-2 text-right md-margin-bottom-10">
<button type="button" onclick="win_zip('fviewcomment', 'map_zip', 'map_addr1', 'map_addr2', 'map_addr3', 'map_addr_jibeon');" class="btn-e btn-e-dark">주소 검색</button>
</div>
<div class="col col-6 inline-group">
<label class="radio" for="map_type_1">
<input type="radio" name="map_type" id="map_type_1" value="1" checked="checked"><i class="rounded-x"></i> Google지도
</label>
<label class="radio" for="map_type_2">
<input type="radio" name="map_type" id="map_type_2" value="2"><i class="rounded-x"></i> 네이버지도
</label>
<label class="radio" for="map_type_3">
<input type="radio" name="map_type" id="map_type_3" value="3"><i class="rounded-x"></i> 다음지도
</label>
</div>
</div>
<div class="margin-bottom-10"></div>
<div class="row">
<div class="col col-10">
<label class="input">
<input type="text" name="map_addr1" id="map_addr1" size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 기본주소</div>
</div>
</div>
<div class="row">
<div class="col col-5">
<label class="input">
<input type="text" name="map_addr2" id="map_addr2" size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 상세주소</div>
</div>
<div class="col col-5">
<label class="input">
<input type="text" name="map_name" id="map_name" size="50">
</label>
<div class="note margin-bottom-10"><strong>Note:</strong> 장소명</div>
</div>
<input type="hidden" name="map_addr3" id="map_addr3" value="">
<input type="hidden" name="map_addr_jibeon" value="">
<div class="col col-2 text-right">
<a href="javascript:;" class="btn-e btn-e-yellow" style="width:60px;padding:5px 12px;text-align: center;" id="btn_map" onclick="return false;">적용</a>
</div>
</div>
</div>
</div>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_coding"]=='1'){?>
<div id="collapse-code-cm" class="panel-collapse collapse">
<div class="comment-function-box">
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">HTML</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">PHP</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">CSS</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">JS</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">JAVA</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">XML</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">PYTHON</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">RUBY</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">SASS</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">SCALA</a>
<a href="javascript:;" class="ch_code btn-e-xs" onclick="return false;">SQL</a>
</div>
</div>
<?php }?>
</div>
</div>
<div class="margin-bottom-10"></div>
<label class="textarea textarea-resizable">
<?php if($GLOBALS["comment_min"]||$GLOBALS["comment_max"]){?><strong id="char_cnt"><span id="char_count"></span>글자</strong><?php }?>
<textarea rows="7" id="wr_content" name="wr_content" maxlength="10000" required title="내용" <?php if($GLOBALS["comment_min"]||$GLOBALS["comment_max"]){?>onkeyup="check_byte('wr_content', 'char_count');"<?php }?>><?php echo $GLOBALS["c_wr_content"]?></textarea>
<?php if($GLOBALS["comment_min"]||$GLOBALS["comment_max"]){?><script> check_byte('wr_content', 'char_count'); </script><?php }?>
<script>
$("textarea#wr_content[maxlength]").live("keyup change", function() {
var str = $(this).val()
var mx = parseInt($(this).attr("maxlength"))
if (str.length > mx) {
$(this).val(str.substr(0, mx));
return false;
}
});
</script>
</label>
<div class="note"><strong>Note:</strong> 댓글은 자신을 나타내는 얼굴입니다. 무분별한 댓글, 욕설, 비방 등을 삼가하여 주세요.</div>
</section>
<?php if(!$GLOBALS["is_member"]){?>
<section>
<label class="label">자동등록방지</label>
<div class="vc-captcha"><?php echo $GLOBALS["captcha_html"]?></div>
</section>
<?php }?>
</div>
<div class="comment-write-submit">
<button type="submit" id="btn_submit" class="btn-e btn-e-lg btn-e-yellow" value="댓글등록">댓글등록</button>
</div>
</form>
</div>
<?php }?>
<div class="margin-hr-15"></div>
</div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<?php }?>
<div id="map_canvas" style="width:1000px;height:400px;display:none"></div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_cmt_infinite"]=='1'){?>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/masonry/jquery.masonry.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/infinite-scroll/jquery.infinitescroll.min.js"></script>
<?php }?>
<script>
var save_before = '';
var save_html = document.getElementById('view-comment-write').innerHTML;
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
function set_emoticon(emoticon) {
var type='emoticon';
set_textarea_contents(type,emoticon);
}
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
function set_map_address(map_type, map_addr1, map_addr2, map_name) {
geocoder = new google.maps.Geocoder();
var subgps;
var latlng = new google.maps.LatLng('');
var myOptions = {
zoom: 16,
center: latlng,
mapTypeId: google.maps.MapTypeId.ROADMAP
};
gmap = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
var address = map_addr1 + " " + map_addr2;
geocoder.geocode( { 'address': address}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
gmap.setCenter(results[0].geometry.location);
subgps = gmap.getCenter();
set_textarea_contents('map', map_type+'^|^'+address+'^|^'+map_name+'^|^'+subgps);
} else {
alert("잘못된 주소입니다.");
}
});
}
<?php }?>
function set_textarea_contents(type,value) {
var type_text = '';
var content = '';
var mobile = <?php if(G5_IS_MOBILE){?>true<?php }else{?>false<?php }?>;
switch(type) {
case 'emoticon': type_text = '이모티콘'; break;
case 'video': type_text = '동영상'; break;
case 'code': type_text = 'code'; break;
case 'sound': type_text = 'soundcloud'; break;
case 'map': type_text = '지도'; break;
}
if(type_text != 'code') {
content = '{'+type_text+':'+value+'}';
} else {
content = '{code:'+value+'}\n\n{/code}\n'
}
var wr_html = $("#wr_content").val();
var wr_emo = content;
wr_html += wr_emo;
$("#wr_content").val(wr_html);
}
function good_and_write()
{
var f = document.fviewcomment;
if (fviewcomment_submit(f)) {
f.is_good.value = 1;
f.submit();
} else {
f.is_good.value = 0;
}
}
function fviewcomment_submit(f) {
var pattern = /(^\s*)|(\s*$)/g; // \s 공백 문자
f.is_good.value = 0;
<?php if($GLOBALS["is_anonymous"]){?>
var wr_1 = '<?php echo $GLOBALS["wr_1"]?>';
if($("#anonymous").is(':checked')) {
wr_1 = wr_1+'|y';
f.wr_1.value=wr_1;
}
<?php }?>
var subject = "";
var content = "";
$.ajax({
url: g5_bbs_url+"/ajax.filter.php",
type: "POST",
data: {
"subject": "",
"content": f.wr_content.value
},
dataType: "json",
async: false,
cache: false,
success: function(data, textStatus) {
subject = data.subject;
content = data.content;
}
});
if (content) {
alert("내용에 금지단어('"+content+"')가 포함되어있습니다");
f.wr_content.focus();
return false;
}
// 양쪽 공백 없애기
var pattern = /(^\s*)|(\s*$)/g; // \s 공백 문자
document.getElementById('wr_content').value = document.getElementById('wr_content').value.replace(pattern, "");
if (char_min > 0 || char_max > 0)
{
check_byte('wr_content', 'char_count');
var cnt = parseInt(document.getElementById('char_count').innerHTML);
if (char_min > 0 && char_min > cnt)
{
alert("댓글은 "+char_min+"글자 이상 쓰셔야 합니다.");
return false;
} else if (char_max > 0 && char_max < cnt)
{
alert("댓글은 "+char_max+"글자 이하로 쓰셔야 합니다.");
return false;
}
}
else if (!document.getElementById('wr_content').value)
{
alert("댓글을 입력하여 주십시오.");
return false;
}
if (typeof(f.wr_name) != 'undefined')
{
f.wr_name.value = f.wr_name.value.replace(pattern, "");
if (f.wr_name.value == '')
{
alert('이름이 입력되지 않았습니다.');
f.wr_name.focus();
return false;
}
}
if (typeof(f.wr_password) != 'undefined')
{
f.wr_password.value = f.wr_password.value.replace(pattern, "");
if (f.wr_password.value == '')
{
alert('비밀번호가 입력되지 않았습니다.');
f.wr_password.focus();
return false;
}
}
<?php if($is_guest) echo chk_captcha_js();  ?>
set_comment_token(f);
document.getElementById("btn_submit").disabled = "disabled";
return true;
}
function comment_box(comment_id, work)
{
var el_id;
// 댓글 아이디가 넘어오면 답변, 수정
if (comment_id)
{
if (work == 'c')
el_id = 'reply_' + comment_id;
else
el_id = 'edit_' + comment_id;
}
else
el_id = 'view-comment-write';
if (save_before != el_id)
{
if (save_before)
{
document.getElementById(save_before).style.display = 'none';
document.getElementById(save_before).innerHTML = '';
}
document.getElementById(el_id).style.display = '';
document.getElementById(el_id).innerHTML = save_html;
// 댓글 수정
if (work == 'cu')
{
document.getElementById('wr_content').value = document.getElementById('save_comment_' + comment_id).value;
if (typeof char_count != 'undefined')
check_byte('wr_content', 'char_count');
if (document.getElementById('secret_comment_'+comment_id).value)
document.getElementById('wr_secret').checked = true;
else
document.getElementById('wr_secret').checked = false;
<?php if($GLOBALS["is_anonymous"]){?>
if (document.getElementById('anonymous_id_'+comment_id).value)
document.getElementById('anonymous').checked = true;
else
document.getElementById('anonymous').checked = false;
<?php }?>
var imgname = document.getElementById('imgname_' + comment_id).value;
if(imgname) {
var delchk_str = '<label class="checkbox"><input type="checkbox" name="del_cmtimg" value="1"><i></i><span class="font-size-12">파일삭제 ('+imgname+')</span></label>';
$("#del_cmtimg").html('');
$("#del_cmtimg").html(delchk_str);
}
}
document.getElementById('comment_id').value = comment_id;
document.getElementById('w').value = work;
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
$(".emoticon").venobox({border:'3px'});
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
//동영상 추가
$("#btn_video").click(function(){
var v_url = $("#video_url").val();
if(!v_url) alert('동영상 주소를 입력해 주세요.');
else set_textarea_contents('video',v_url);
$("#video_url").val('');
});
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_coding"]=='1'){?>
//코드 추가
$(".ch_code").click(function(){
var ch = $(this).text();
var val = ch.toLowerCase();
set_textarea_contents('code',val);
});
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
//사운드크라우드 추가
$("#btn_scloud").click(function(){
var s_url = $("#scloud_url").val();
if(!s_url) alert('사운드크라우드 주소를 입력해 주세요.');
else set_textarea_contents('sound',s_url);
$("#scloud_url").val('');
});
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
//지도 추가
$("#btn_map").click(function(){
var map_type = $("input[name='map_type']:checked").val();
var map_addr1 = $("#map_addr1").val();
var map_addr2 = $("#map_addr2").val();
var map_name = $("#map_name").val();
set_map_address(map_type, map_addr1, map_addr2, map_name);
});
<?php }?>
if(save_before)
$("#captcha_reload").trigger("click");
save_before = el_id;
}
}
function comment_delete()
{
return confirm("이 댓글을 삭제하시겠습니까?");
}
comment_box('', 'c'); // 댓글 입력폼이 보이도록 처리하기위해서 추가 (root님)
<?php if($TPL_VAR["board"]["bo_use_sns"]&&($TPL_VAR["config"]["cf_facebook_appid"]||$TPL_VAR["config"]["cf_twitter_key"])){?>
// sns 등록
$(function() {
$("#bo_vc_send_sns").load(
"<?php echo G5_SNS_URL?>/view_comment_write.sns.skin.php?bo_table=<?php echo $GLOBALS["bo_table"]?>",
function() {
save_html = document.getElementById('view-comment-write').innerHTML;
}
);
});
<?php }?>
</script>
<script>
$(function() {
// 댓글 추천, 비추천
$(".goodcmt_button, .nogoodcmt_button").click(function() {
var $tx;
if($(this).attr('title') == "goodcmt")
$tx = $(".board-cmt-act-good");
else
$tx = $(".board-cmt-act-nogood");
excute_goodcmt(this.href, $(this), $tx);
return false;
});
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
// 신고버튼 클릭시, 댓글 cmt_id 설정
$(".cmt_yellow_card, .cancel_cmt_yellow_card").click(function() {
var cmt_id = $(this).attr('data-cmt-id');
$(".yellowcard-modal #modal_cmt_id").val(cmt_id);
});
<?php }?>
});
function excute_goodcmt(href, $el, $tx)
{
$.post(
href,
{ js: "on" },
function(data) {
if(data.error) {
alert(data.error);
return false;
}
if(data.count) {
$el.find("strong").text(number_format(String(data.count)));
if($tx.attr("id").search("nogood") > -1) {
$tx.text("이 글을 비추천하셨습니다.");
$tx.fadeIn(200).delay(2500).fadeOut(200);
} else {
$tx.text("이 글을 추천하셨습니다.");
$tx.fadeIn(200).delay(2500).fadeOut(200);
}
}
}, "json"
);
}
</script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_cmt_infinite"]=='1'){?>
<script>
$(document).ready(function(){
var $container = $('.view-comment');
$container.infinitescroll({
navSelector  : "#infinite_pagination",
nextSelector : "#infinite_pagination .next",
itemSelector : ".view-comment-item",
loading: {
finishedMsg: 'END',
img: '/eyoom/theme/basic2/image/loading.gif'
}
},
function( newElements ) {
var $newElems = $( newElements ).css({ opacity: 0 });
$newElems.imagesLoaded(function(){
$newElems.animate({ opacity: 1 });
$container.append($newElems);
});
});
$(window).unbind('.infscr');
$('#view-comment-more').click(function(){
$container.infinitescroll('retrieve');
$('#infinite_pagination').show();
return false;
});
});
</script>
<?php }?>