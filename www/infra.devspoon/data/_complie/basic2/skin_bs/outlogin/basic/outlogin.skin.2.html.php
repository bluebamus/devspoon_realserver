<?php /* Template_ 2.2.8 2016/11/03 01:18:26 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/skin_bs/outlogin/basic/outlogin.skin.2.html 000008403 */ ?>
<?php if (!defined("_GNUBOARD_")) exit; ?>
<section class="ol-after">
<div class="ol-member-box">
<div class="member-photo">
<div class="photo">
<a href="#" class="member-img" data-toggle="modal" data-target=".profile-modal"><?php if($TPL_VAR["profile_photo"]){?><?php echo $TPL_VAR["profile_photo"]?><?php }else{?><span class="ol-user-icon"><i class="fa fa-user"></i></span><?php }?></a>
<?php if( 0){?>
<a href="<?php echo G5_BBS_URL?>/respond.php"><span class="badge rounded-2x badge-red tooltips" data-placement="top" data-toggle="tooltip" data-original-title="내글반응"><i class="fa fa-commenting"></i> <?php echo $TPL_VAR["respond"]?></span></a>
<?php }?>
</div>
</div>
<!-- Small modal -->
<div class="modal fade profile-modal" tabindex="-1" role="dialog" aria-labelledby="profilelModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<form name="profile_photo" method="post" action="<?php echo EYOOM_CORE_URL?>/member/photo_update.php" enctype="multipart/form-data" class="eyoom-form">
<input type="hidden" name="old_photo" value="<?php echo $TPL_VAR["eyoomer"]["photo"]?>">
<input type="hidden" name="back_url" value="<?php echo $_SERVER["REQUEST_URI"]?>">
<div class="modal-header">
<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
<h5 id="profilelModalLabel" class="modal-title"><strong>프로필 사진변경</strong></h5>
</div>
<div class="modal-body">
<div class="profile-photo"><?php echo $TPL_VAR["profile_photo"]?></div>
<div class="profile-txt">프로필 사진은 이미지(gif/jpg/png) 파일만 등록가능하며 자동으로 가로x세로 80x80픽셀로 썸네일화 합니다.</div>
<label class="label">사진 선택</label>
<label for="file" class="input input-file">
<div class="button bg-color-light-grey"><input type="file" id="file" name="photo" value="사진선택" onchange="this.parentNode.nextSibling.value = this.value">파일선택</div><input type="text" readonly>
</label>
<?php if($TPL_VAR["eyoomer"]["photo"]){?><label class="checkbox"><input type="checkbox" name="del_photo" value="1"><i></i>프로필사진 삭제</label><?php }?>
</div>
<div class="modal-footer">
<button data-dismiss="modal" class="btn-e btn-e-dark" type="button">닫기</button>
<button class="btn-e" type="submit" value="저장하기">저장하기</button>
</div>
</form>
</div>
</div>
</div>
<iframe name="photoframe" id="photoframe" style="display:none;"></iframe>
<!-- End Small Modal -->
<div class="member-info">
<div class="member-name">
<h6>
<?php if($TPL_VAR["lv"]["gnu_icon"]){?>
<span style='display:inline-block;margin-right:2px;'><img src="<?php echo $TPL_VAR["lv"]["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_VAR["lv"]["eyoom_icon"]){?>
<span style='display:inline-block;margin-right:2px;'><img src="<?php echo $TPL_VAR["lv"]["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
<strong><?php echo $TPL_VAR["nick"]?></strong> <span class="font-size-11 pull-right"><?php if($GLOBALS["is_admin"]){?>최고관리자<?php }else{?><?php echo $TPL_VAR["lvinfo"]["gnu_name"]?>/<?php echo $TPL_VAR["lvinfo"]["name"]?><?php }?></span>
</h6>
</div>
<div class="member-point">
<a href="<?php echo G5_BBS_URL?>/point.php" target="_blank" id="ol_after_pt">
<i class="fa fa-tachometer"></i>
<span class="service-heading"><?php echo $TPL_VAR["levelset"]["gnu_name"]?> :</span>
<span><?php echo $TPL_VAR["point"]?></span>
</a>
</div>
<div class="member-lv">
<div class="width-50 pull-left">
<p class="margin-bottom-0">레벨</p>
<p class="color-red"><?php echo $TPL_VAR["eyoomer"]["level"]?></p>
</div>
<div class="widht-50 pull-right text-right">
<p class="margin-bottom-0"><?php echo $TPL_VAR["levelset"]["eyoom_name"]?></p>
<p class="color-red"><?php echo number_format($TPL_VAR["eyoomer"]["level_point"])?></p>
</div>
<div class="clearfix"></div>
</div>
<div class="member-statistics">
<p class="heading-xs">Progress Bar <span class="pull-right"><?php echo $TPL_VAR["lvinfo"]["ratio"]?>%</span></hp>
<div class="progress progress-e progress-xxs">
<div style="width: <?php echo $TPL_VAR["lvinfo"]["ratio"]?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $TPL_VAR["lvinfo"]["ratio"]?>" role="progressbar" class="progress-bar progress-bar-e">
</div>
</div>
</div>
</div>
</div>
</section>
<section class="ol-after-bottom">
<div class="width-50 pull-left">
<div class="btn-group">
<button type="button" class="btn-e btn-e-dark dropdown-toggle" data-toggle="dropdown">내메뉴</button>
<button type="button" class="btn-e btn-e-dark btn-e-split-dark dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-sort"></i>
<span class="sr-only">Toggle Dropdown</span>
</button>
<ul class="dropdown-menu" role="menu">
<li><a href="<?php echo G5_BBS_URL?>/respond.php">내글반응 <span class="badge badge-red rounded-2x"><?php echo $TPL_VAR["respond"]?></span></a></li>
<li>
<a href="<?php echo G5_BBS_URL?>/memo.php" target="_blank" id="ol_after_memo" class="win_memo">
쪽지 <span class="badge badge-red rounded-2x"><?php echo $TPL_VAR["memo_not_read"]?></span>
</a>
</li>
<li>
<a href="<?php echo G5_BBS_URL?>/scrap.php" target="_blank" id="ol_after_scrap" class="win_scrap">
스크랩
</a>
</li>
<?php if($GLOBALS["is_admin"]=='super'||$GLOBALS["is_auth"]){?>
<li class="divider"></li>
<li><a href="<?php echo G5_ADMIN_URL?>">관리자페이지</a></li>
<?php }?>
</ul>
</div>
</div>
<div class="width-50 pull-right text-right">
<a href="<?php echo G5_BBS_URL?>/logout.php" type="button" class="btn-e">로그아웃</a>
</div>
<div class="clearfix"></div>
</section>
<style>
.ol-after {position:relative;display:block;border:1px solid #e5e5e5;background:#fff;padding:0;margin-top:50px;font-size:12px}
.ol-after-bottom {position:relative;display:block;border:1px solid #e5e5e5;border-top:0;background:#fff;padding:10px;font-size:12px}
.ol-after .member-photo .member-img {z-index:1;position:absolute;top:-30px;left:50%;margin-left:-30px;height:60px;width:60px;padding:3px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;box-shadow:0 0 0 1px rgba(0, 0, 0, 0.12);background-color:#fff}
.ol-after .member-photo .member-img img {height:54px;width:54px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;background-color:#fff;background-size:cover}
.ol-after .member-photo .member-img i {height:54px;width:54px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;background-color:#a4a4aa;color:#fff;font-size:34px;text-align:center;line-height:54px}
.ol-after .member-photo .photo .badge {position:absolute;top:-30px;left:50%;margin-left:30px;font-size:11px}
.ol-after .profile-photo {position:relative;display:block;text-align:center;margin:10px auto 20px;height:80px;width:80px;padding:4px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;box-shadow:0 0 0 1px rgba(0, 0, 0, 0.12);background-color:#fff}
.ol-after .profile-photo i {height:80px;width:80px;color:#84848a;font-size:50px;text-align:center;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;background-color:#fff;line-height:72px}
.ol-after .profile-photo img {height:80px;width:80px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;background-color:#fff;background-size:cover}
.ol-after .profile-txt {margin-bottom:20px;font-size:12px}
.ol-after .member-info {position:relative;overflow:hidden;padding:30px 10px 0}
.ol-after .member-name {border-bottom:1px dotted #e5e5e5;margin-bottom:5px}
.ol-after .member-point {border-bottom:1px dotted #e5e5e5;padding-bottom:5px;margin-bottom:5px}
.ol-after .member-point i {font-size:20px;color:#ff9501}
.ol-after .member-point span {margin-left:5px}
.ol-after .member-lv {margin-bottom:7px}
.ol-after .member-statistics p {margin-bottom:5px;font-size:11px}
</style>
<script>
// 탈퇴의 경우 아래 코드를 연동하시면 됩니다.
function member_leave()
{
if (confirm("정말 회원에서 탈퇴 하시겠습니까?"))
location.href = "<?php echo G5_BBS_URL?>/member_confirm.php?url=member_leave.php";
}
</script>