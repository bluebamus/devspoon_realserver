<?php /* Template_ 2.2.8 2016/11/03 01:18:24 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/skin_bs/newpost/tab_newpost/latest.skin.html 000002299 */ 
$TPL_write_1=empty($TPL_VAR["write"])||!is_array($TPL_VAR["write"])?0:count($TPL_VAR["write"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="tab_newpost">
<?php if($TPL_write_1){foreach($TPL_VAR["write"] as $TPL_V1){?>
<article class="new-post">
<a href="<?php echo $TPL_V1["href"]?>">
<div class="post-subject">
<?php if($TPL_V1["wr_comment"]){?><span class="post-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span> <?php }?><?php echo $TPL_V1["wr_subject"]?>
</div>
<span class="post-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="post-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
<span class="post-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<span class="post-time"><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?></span>
</a>
</article>
<?php }}else{?>
<p class="text-center font-size-12 margin-top-10">최신글이 없습니다.</p>
<?php }?>
</div>
<style>
.new-post{position:relative;overflow:hidden;padding:8px 0;border-top:1px solid #eee}
.new-post:first-child{border-top:none}
.new-post .margin-hr-5 {height:1px;border-top:1px dotted #eee;margin:5px 0}
.new-post .post-subject {display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;font-size:12px}
.new-post a:hover .post-subject {text-decoration:underline}
.new-post .post-comment {display:inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#74747a}
.new-post .post-photo img {width:20px;height:20px;margin-right:2px}
.new-post .post-photo .post-user-icon {width:20px;height:20px;font-size:14px;line-height:20px;text-align:center;background:#84848a;color:#fff;margin-right:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.new-post .post-nick {font-size:11px;color:#777}
.new-post .post-time {font-size:11px;color:#777;margin-left:5px}
.new-post .post-time .i-color {color:#bbb}
</style>