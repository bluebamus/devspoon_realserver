<?php /* Template_ 2.2.8 2016/11/03 01:18:24 /home/xn--2o2bq0eztnboo.com/www/eyoom/theme/basic2/skin_bs/newpost/tab_newcomment/latest.skin.html 000002040 */ 
$TPL_comment_1=empty($TPL_VAR["comment"])||!is_array($TPL_VAR["comment"])?0:count($TPL_VAR["comment"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="tab_newcomment">
<?php if($TPL_comment_1){foreach($TPL_VAR["comment"] as $TPL_V1){?>
<article class="new-comment">
<a href="<?php echo $TPL_V1["href"]?>">
<div class="comment-subject">
<?php echo $TPL_V1["wr_subject"]?>
</div>
<span class="comment-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="comment-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
<span class="comment-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<span class="comment-time"><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?></span>
</a>
</article>
<?php }}else{?>
<p class="text-center font-size-12 margin-top-10">최신글이 없습니다.</p>
<?php }?>
</div>
<style>
.new-comment {position:relative;overflow:hidden;padding:8px 0;border-top:1px solid #eee}
.new-comment:first-child {border-top:none}
.new-comment .margin-hr-5 {height:1px;border-top:1px dotted #eee;margin:5px 0}
.new-comment .comment-subject {display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;font-size:12px}
.new-comment a:hover .comment-subject {text-decoration:underline}
.new-comment .comment-photo img {width:20px;height:20px;margin-right:2px}
.new-comment .comment-photo .comment-user-icon {width:20px;height:20px;font-size:14px;line-height:20px;text-align:center;background:#84848a;color:#fff;margin-right:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.new-comment .comment-nick {font-size:11px;color:#777}
.new-comment .comment-time {font-size:11px;color:#777;margin-left:5px}
.new-comment .comment-time .i-color {color:#bbb}
</style>