<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/webzine/latest.skin.html 000004978 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="headline">
<h6><strong><?php echo $TPL_VAR["title"]?></strong></h6>
</div>
<div class="webzine-latest">
<div class="row">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<div class="col-sm-6">
<div class="webzine-item">
<div class="webzine-img">
<a href="<?php echo $TPL_V1["href"]?>">
<div class="img-box">
<?php if($TPL_V1["image"]){?>
<img class="img-responsive" src="<?php echo $TPL_V1["image"]?>">
<?php if($TPL_V1["wr_comment"]){?><span class="img-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?>
<?php if($TPL_V1["is_video"]){?><span class="video-icon"><i class="fa fa-play-circle-o"></i></span><?php }?>
<?php }else{?>
<span class="no-image">No Image</span>
<?php }?>
<div class="img-caption">
<span><i class="fa fa-eye"></i> <?php echo number_format($TPL_V1["wr_hit"])?></span>
<?php if($TPL_V1["wr_good"]){?><span><i class="fa fa-thumbs-up"></i> <?php echo number_format($TPL_V1["wr_good"])?></span><?php }?>
<?php if($TPL_V1["wr_nogood"]){?><span><i class="fa fa-thumbs-down"></i> <?php echo number_format($TPL_V1["wr_nogood"])?></span><?php }?>
</div>
</div>
</a>
</div>
<div class="webzine-txt">
<a href="<?php echo $TPL_V1["href"]?>">
<div class="txt-subj">
<h5><?php echo $TPL_V1["wr_subject"]?></h5>
</div>
<?php if($TPL_VAR["content"]=='y'){?>
<p class="txt-cont"><?php echo $TPL_V1["wr_content"]?></p>
<?php }?>
<span class="txt-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="txt-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<span class="txt-time"><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?></span>
</a>
</div>
</div>
</div>
<?php }}else{?>
<p class="text-center font-size-12 margin-top-30">최신글이 없습니다.</p>
<?php }?>
</div>
</div>
<script>
$(function(){
var duration = 120;
var $img_cap = $('.webzine-latest .webzine-img');
$img_cap.find('.img-box')
.on('mouseover', function(){
$(this).find('.img-caption').stop(true).animate({bottom: '0px'}, duration);
})
.on('mouseout', function(){
$(this).find('.img-caption').stop(true).animate({bottom: '-26px'}, duration);
});
});
</script>
<style>
.webzine-latest {position:relative;overflow:hidden;border:1px solid #e5e5e5;padding:15px 15px 0px;min-height:207px}
.webzine-item {position:relative;overflow:hidden;margin-bottom:15px}
.webzine-latest .webzine-img {position:relative;overflow:hidden;float:left;width:40%}
.webzine-latest .img-box {position:relative;overflow:hidden;height:98px;background:#34343a;line-height:98px;text-align:center}
.webzine-latest .img-box .no-image {color:#888;font-size:11px}
.webzine-latest .img-comment {position:absolute;top:8px;left:8px;display:inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background:#74747a}
.webzine-latest .img-box .video-icon {position:absolute;top:5px;right:5px;color:#fff;font-size:20px;line-height:20px}
.webzine-latest .img-caption {color:#fff;font-size:11px;position:absolute;left:0;bottom:-26px;display:block;z-index:1;background:rgba(0, 0, 0, 0.7);text-align:left;width:100%;height:26px;line-height:20px;margin-bottom:0;padding:3px 10px}
.webzine-latest .img-caption span {margin-right:7px}
.webzine-latest .img-caption span i {color:#aaa}
.webzine-latest .webzine-txt {position:relative;overflow:hidden;float:right;padding-left:10px;width:60%}
.webzine-latest .txt-subj {margin-bottom:5px}
.webzine-latest .txt-subj h5 {font-size:12px;font-weight:bold;margin:0;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden}
.webzine-latest .webzine-txt a:hover .txt-subj h5 {color:#ff2a00;text-decoration:underline}
.webzine-latest .txt-cont {position:relative;overflow:hidden;height:34px;font-size:11px;color:#888;margin-bottom:10px}
.webzine-latest .txt-photo img {width:20px;height:20px;margin-right:2px}
.webzine-latest .txt-photo .txt-user-icon {width:20px;height:20px;font-size:14px;line-height:20px;text-align:center;background:#84848a;color:#fff;margin-right:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.webzine-latest .txt-nick {font-size:11px;color:#555}
.webzine-latest .txt-time {font-size:11px;color:#555;margin-left:5px}
.webzine-latest .txt-time .i-color {color:#bbb}
@media (max-width: 1199px) {
.webzine-latest .img-box {height:80px}
.webzine-latest .txt-cont {height:17px}
}
</style>