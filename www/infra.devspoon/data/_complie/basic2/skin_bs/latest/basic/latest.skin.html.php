<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/basic/latest.skin.html 000002932 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="headline">
<h6><strong><?php echo $TPL_VAR["title"]?></strong></h6>
</div>
<div class="basic-latest">
<ul class="list-unstyled">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<li>
<a href="<?php echo $TPL_V1["href"]?>">
<div class="basic-subj">
- <?php if($TPL_V1["wr_comment"]){?><span class="basic-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span> <?php }?><?php echo $TPL_V1["wr_subject"]?>
</div>
<?php if( 1){?>
<div class="basic-time">
<i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('m-d',$TPL_V1["datetime"])?>
</div>
<?php }else{?>
<div class="basic-member">
<div class="basic-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="basic-user-icon"><i class="fa fa-user"></i></span><?php }?></div>
<div class="basic-nick"><?php echo $TPL_V1["mb_nick"]?></div>
</div>
<?php }?>
</a>
</li>
<?php }}else{?>
<li><p class="text-center font-size-12 margin-top-30">최신글이 없습니다.</p></li>
<?php }?>
</ul>
</div>
<style>
.basic-latest {position:relative;overflow:hidden;border:1px solid #e5e5e5;padding:15px;min-height:262px}
.basic-latest ul {margin-bottom:0}
.basic-latest li {position:relative;overflow:hidden;padding:2px 0;font-size:12px}
.basic-latest .basic-subj {position:relative;width:70%;padding-right:0;padding-left:0;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:left}
.basic-latest .basic-comment {display: inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#74747a}
.basic-latest .basic-time {position:relative;overflow:hidden;width:30%;font-size:12px;text-align:right;color:#555;float:right}
.basic-latest .basic-time .i-color {color:#bbb}
.basic-latest .basic-member {position:relative;overflow:hidden;display:inline-block;width:30%;float:right}
.basic-latest .basic-photo img {width:17px;height:17px;margin-left:7px;float:right}
.basic-latest .basic-photo .basic-user-icon {width:17px;height:17px;font-size:11px;line-height:17px;text-align:center;background:#84848a;color:#fff;margin-left:7px;float:right}
.basic-latest .basic-nick {font-size:12px;color:#888;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:right}
.basic-latest a:hover .basic-subj {text-decoration:underline}
.basic-latest a:hover .basic-nick {color:#000}
.basic-latest a:hover .basic-time i {color:#ff2a00}
</style>