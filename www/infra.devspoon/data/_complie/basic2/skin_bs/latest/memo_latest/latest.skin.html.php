<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/memo_latest/latest.skin.html 000004631 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic2/plugins/scrollbar/src/perfect-scrollbar.css" id="style_color" type="text/css" media="screen">',0);
?>
<li class="dropdown dropdown-extended dropdown-memo">
<a class="dropdown-toggle" data-toggle="dropdown" <?php if( 1){?>data-hover="dropdown"<?php }?> data-target="#" style="cursor:pointer">
쪽지
<span class="badge rounded-2x badge-e badge-dark"><?php echo $GLOBALS["memo_not_read"]?></span>
</a>
<ul class="dropdown-menu">
<li class="external">
<h5>미확인 <strong class="color-red"><?php echo $GLOBALS["memo_not_read"]?>건</strong> 쪽지가 있습니다.</h5>
<a href="<?php echo G5_BBS_URL?>/memo.php" target="_blank" class="win_memo">more</a>
</li>
<li>
<ul class="dropdown-menu-list contentHolder" style="height: 275px;">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<?php if($TPL_V1["is_read"]){?>
<li class="read-disable">
<?php }else{?>
<li>
<?php }?>
<a href="<?php echo $TPL_V1["href"]?>" target="_blank" class="win_memo">
<?php if($TPL_VAR["photo"]=='y'){?>
<span class="photo">
<?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="user_icon"><i class="fa fa-user memo-photo"></i></span><?php }?>
</span>
<?php }?>
<span class="description">
<span class="from color-black"><?php echo $TPL_V1["mb_name"]?></span>
</span>
<span class="subject"><?php echo $TPL_V1["memo"]?></span>
<span class="time"><?php if($TPL_V1["is_read"]){?>읽음<?php }else{?><strong class="color-red">읽지않음</strong><?php }?> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d H:i',$TPL_V1["datetime"])?></span>
<div class="clearfix"></div>
</a>
</li>
<?php }}else{?>
<li><p class="text-center margin-top-20 font-size-12">확인하지 않은 쪽지가 없습니다.</p></li>
<?php }?>
</ul>
</li>
</ul>
</li>
<style>
.header-topbar li.dropdown-memo .dropdown-menu .dropdown-menu-list > li a .details {overflow:hidden}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li {position:relative;overflow:hidden;min-width:248px}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li a {border-top:1px dotted #e5e5e5}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li.read-disable a {color:#bbb !important}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li.read-disable span {color:#bbb !important}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li:first-child a {border-top:0}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo {float:left;margin:0 10px 10px 0}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo img {height:28px;width:28px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;-ms-border-radius:50% !important;-o-border-radius:50% !important;border-radius:50% !important}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo .memo-photo {height:28px;width:28px;font-size:16px;line-height:28px;text-align:center;background:#eee;color:#84848a;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;-ms-border-radius:50% !important;-o-border-radius:50% !important;border-radius:50% !important}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .description {display:block;margin-left:38px}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .description .from {font-size:12px;font-weight:600}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .subject {display:block !important;font-size:12px;line-height:1.3;margin-left:38px;margin-top:5px}
.header-topbar li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li a:hover .subject {text-decoration:underline}
.header-topbar li.dropdown-memo .dropdown-menu .dropdown-menu-list > li a .time {display:block !important;font-size:10px;color:#999;float:right;margin-top:5px}
</style>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/eyoom/theme/basic2/plugins/scrollbar/src/perfect-scrollbar.js"></script>
<script>
jQuery(document).ready(function ($) {
"use strict";
$('.contentHolder').perfectScrollbar();
});
</script>