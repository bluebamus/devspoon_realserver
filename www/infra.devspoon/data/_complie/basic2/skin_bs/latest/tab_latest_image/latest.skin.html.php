<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/tab_latest_image/latest.skin.html 000003723 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="tab-latest-image">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<div class="img-item-wrap">
<div class="img-item">
<a href="<?php echo $TPL_V1["href"]?>">
<div class="img-box">
<?php if($TPL_V1["image"]){?>
<img class="img-responsive" src="<?php echo $TPL_V1["image"]?>">
<?php if($TPL_V1["wr_comment"]){?><span class="img-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?>
<?php if($TPL_V1["is_video"]){?><span class="video-icon"><i class="fa fa-play-circle-o"></i></span><?php }?>
<?php }else{?>
<span class="no-image">No Image</span>
<?php }?>
<div class="img-caption">
<div class="img-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="img-user-icon"><i class="fa fa-user"></i></span><?php }?></div>
<div class="img-nick"><?php echo $TPL_V1["mb_nick"]?></div>
</div>
</div>
<div class="img-subj">
<h5><?php if($TPL_V1["new"]){?><i class="fa fa-check-circle color-red"></i><?php }?><?php echo $TPL_V1["wr_subject"]?></h5>
</div>
</a>
</div>
</div>
<?php }}else{?>
<p class="text-center font-size-12 margin-top-30">최신글이 없습니다.</p>
<?php }?>
</div>
<script>
$(function(){
var duration = 120;
var $img_cap = $('.tab-latest-image .img-item');
$img_cap.find('.img-box')
.on('mouseover', function(){
$(this).find('.img-caption').stop(true).animate({bottom: '0px'}, duration);
})
.on('mouseout', function(){
$(this).find('.img-caption').stop(true).animate({bottom: '-26px'}, duration);
});
});
</script>
<style>
.tab-latest-image .img-item-wrap {position:relative;width:20%;float:left}
.tab-latest-image .img-item {position:relative;overflow:hidden;margin:3px}
.tab-latest-image .img-box {position:relative;overflow:hidden;height:98px;background:#34343a;line-height:98px;text-align:center}
.tab-latest-image .img-box .no-image {color:#888;font-size:11px}
.tab-latest-image .img-comment {position:absolute;top:8px;left:8px;display:inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background:#74747a}
.tab-latest-image .img-box .video-icon {position:absolute;top:5px;right:5px;color:#fff;font-size:20px;line-height:20px}
.tab-latest-image .img-caption {color:#fff;font-size:11px;position:absolute;left:0;bottom:-26px;display:block;z-index:1;background:rgba(0, 0, 0, 0.7);text-align:left;width:100%;height:26px;line-height:20px;margin-bottom:0;padding:3px 5px}
.tab-latest-image .img-photo img {width:20px;height:20px;margin-right:7px;float:left}
.tab-latest-image .img-photo .img-user-icon {width:20px;height:20px;font-size:14px;line-height:20px;text-align:center;background:#84848a;color:#fff;margin-right:7px;float:left}
.tab-latest-image .img-nick {font-size:11px;color:#fff;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:left}
.tab-latest-image .img-subj h5 {color:#000;font-size:12px;display:block;overflow:hidden;white-space:nowrap;word-wrap:normal;text-overflow:ellipsis}
.tab-latest-image a:hover .img-subj h5 {color:#ff2a00;text-decoration:underline}
@media (max-width: 1199px) {
.tab-latest-image .img-item-wrap {width:25%}
}
@media (max-width: 767px) {
.tab-latest-image .img-item-wrap {width:33.33333%}
}
@media (max-width: 540px) {
.tab-latest-image .img-item-wrap {width:50%}
}
</style>