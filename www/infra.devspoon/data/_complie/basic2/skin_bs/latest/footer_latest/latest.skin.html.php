<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/footer_latest/latest.skin.html 000002059 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="footer-latest-wrap">
<ul class="list-unstyled footer-latest">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<li>
<a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["wr_subject"]?></a>
<small><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d H:i',$TPL_V1["datetime"])?><?php if($TPL_V1["wr_comment"]){?>&nbsp;&nbsp;&nbsp;<i class="fa fa-comments-o"></i> <span class="color-red"><?php echo $TPL_V1["wr_comment"]?></span><?php }?></small>
</li>
<?php }}else{?>
<p class="text-center">최신글이 없습니다.</p>
<?php }?>
</ul>
</div>
<style>
.footer-latest-wrap .footer-latest {margin-bottom:0}
.footer-latest-wrap .footer-latest li {padding:12px 0px}
.footer-latest-wrap .footer-latest li:first-child {padding-top:0 !important;border-top:none !important}
.footer-latest-wrap .footer-latest li a {font-size:12px;margin-bottom:3px;display:block;overflow:hidden;white-space:nowrap;word-wrap:normal;text-overflow:ellipsis}
.footer-latest-wrap .footer-latest li small {display:block}
.footer.footer-light .footer-latest-wrap .footer-latest li {border-top:dotted 1px #ddd}
.footer.footer-light .footer-latest-wrap .footer-latest li a {color:#777}
.footer.footer-light .footer-latest-wrap .footer-latest a:hover {color:#000;text-decoration:underline}
.footer.footer-light .footer-latest-wrap .footer-latest li small {color:#555}
.footer.footer-dark .footer-latest-wrap .footer-latest li {border-top:solid 1px #333}
.footer.footer-dark .footer-latest-wrap .footer-latest li a {color:#888}
.footer.footer-dark .footer-latest-wrap .footer-latest a:hover {color:#bbb}
.footer.footer-dark .footer-latest-wrap .footer-latest li small {color:#666}
</style>