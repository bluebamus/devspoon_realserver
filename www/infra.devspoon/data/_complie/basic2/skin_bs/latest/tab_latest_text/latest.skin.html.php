<?php /* Template_ 2.2.8 2020/03/31 21:22:40 /www/infra.devspoon/eyoom/theme/basic2/skin_bs/latest/tab_latest_text/latest.skin.html 000002876 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="tab-latest-text">
<ul class="list-unstyled">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<li>
<a href="<?php echo $TPL_V1["href"]?>">
<div class="txt-subj">
- <?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span> <?php }?><?php echo $TPL_V1["wr_subject"]?>
</div>
<div class="txt-member">
<span class="txt-photo"><?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="txt-user-icon"><i class="fa fa-user"></i></span><?php }?></span>
<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
</div>
<div class="txt-time">
<i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>
</div>
</a>
</li>
<?php }}else{?>
<li><p class="text-center font-size-12 margin-top-30">최신글이 없습니다.</p></li>
<?php }?>
</ul>
</div>
<style>
.tab-latest-text ul {margin-bottom:0}
.tab-latest-text li {position:relative;overflow:hidden;padding:2px 0;font-size:12px}
.tab-latest-text .txt-subj {position:relative;width:70%;padding-right:0;padding-left:0;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:left}
.tab-latest-text .txt-comment {display: inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#74747a}
.tab-latest-text .txt-member {position:relative;overflow:hidden;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;width:15%;float:left}
.tab-latest-text .txt-photo img {width:17px;height:17px;margin-right:2px}
.tab-latest-text .txt-photo .txt-user-icon {width:17px;height:17px;font-size:11px;line-height:17px;text-align:center;background:#84848a;color:#fff;margin-right:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.tab-latest-text .txt-nick {font-size:12px;color:#777}
.tab-latest-text .txt-time {position:relative;overflow:hidden;width:15%;font-size:12px;text-align:right;color:#555;float:left}
.tab-latest-text .txt-time .i-color {color:#bbb}
.tab-latest-text a:hover .txt-subj {text-decoration:underline}
.tab-latest-text a:hover .txt-nick {color:#000}
.tab-latest-text a:hover .txt-time i {color:#ff2a00}
@media (max-width: 650px) {
.tab-latest-text .txt-subj {width:68%}
.tab-latest-text .txt-member {display:none}
.tab-latest-text .txt-time {width:32%}
}
</style>