<?php /* Template_ 2.2.8 2017/07/06 22:45:41 /home/tripfooter.com/www/eyoom/theme/basic2/skin_bs/emoticon/basic/emoticon.skin.html 000001400 */ 
$TPL__emo_type_1=empty($GLOBALS["emo_type"])||!is_array($GLOBALS["emo_type"])?0:count($GLOBALS["emo_type"]);
$TPL__emoticon_1=empty($GLOBALS["emoticon"])||!is_array($GLOBALS["emoticon"])?0:count($GLOBALS["emoticon"]);?>
<?php if (!defined('_GNUBOARD_')) exit;  ?>
<div class="emoticon-list">
<h4>
Emoticons :
<select name="eom" id="emo" onchange="change_emoticon(this.value);">
<?php if($TPL__emo_type_1){foreach($GLOBALS["emo_type"] as $TPL_V1){?>
<option value="<?php echo $TPL_V1?>" <?php if($GLOBALS["emo"]==$TPL_V1){?>selected<?php }?>><?php echo $TPL_V1?></option>
<?php }}?>
</select>
</h4>
<ul class="emoticons">
<?php if($TPL__emoticon_1){foreach($GLOBALS["emoticon"] as $TPL_V1){?>
<li><a href='javascript:;' onclick="set_emoticon('<?php echo $TPL_V1["emoticon"]?>');"><img src="<?php echo $TPL_V1["url"]?>" width="40"></a></li>
<?php }}?>
</ul>
</div>
<style>
.emoticons li{float:left;list-style-type:none;min-height:65px;	margin:5px 16px;}
</style>
<script>
function change_emoticon(emo) {
var url = './emoticon.php?emo='+emo;
$(location).attr('href',url);
}
function set_emoticon(emoticon) {
parent.set_emoticon(emoticon);
parent.jQuery('.vbox-close, .vbox-overlay').trigger('click');
}
</script>
<?php $this->print_("tail_sub",$TPL_SCP,1);?>