<?php //관리자님께 ?>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title><?php echo $config['cf_title']; ?> - 주문 취소 알림 메일</title>
</head>

<?php
$cont_st = 'margin:0 auto 20px;width:94%;border:0;border-collapse:collapse';
$caption_st = 'padding:0 0 5px;font-weight:bold';
$th_st = 'padding:5px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;background:#f5f6fa;text-align:left';
$td_st = 'padding:5px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9';
$empty_st = 'padding:30px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:center';
$ft_a_st = 'display:block;padding:30px 0;background:#484848;color:#fff;text-align:center;text-decoration:none';
?>

<body>

<div style="margin:30px auto;width:600px;border:10px solid #f7f7f7">
    <div style="border:1px solid #dedede">
        <h1 style="margin:0 0 20px;padding:30px 30px 20px;background:#f7f7f7;color:#555;font-size:1.4em">
            <?php echo $config['cf_title'];?> - 취소 주문이 접수되었습니다.
        </h1>

        <p style="<?php echo $cont_st; ?>">
            <strong>주문번호 <?php echo $od_id; ?></strong><br>
            본 메일은 <?php echo G5_TIME_YMDHIS; ?> (<?php echo get_yoil(G5_TIME_YMDHIS); ?>)을 기준으로 작성되었습니다.
        </p>

        <table style="<?php echo $cont_st; ?>">

            <div class="tbl_head01 tbl_wrap">
                <table>
                    <caption>주문 상품 목록</caption>
                    <thead>
                    <tr>
                        <th scope="col">상품명</th>
                        <th scope="col">옵션항목</th>
                        <th scope="col">상태</th>
                        <th scope="col">수량</th>
                        <th scope="col">판매가</th>
                        <th scope="col">소계</th>
                        <th scope="col">쿠폰</th>
                        <th scope="col">포인트</th>
                        <th scope="col">배송비</th>
                        <th scope="col">포인트반영</th>
                        <th scope="col">재고반영</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $chk_cnt = 0;
                    for($i=0; $row=sql_fetch_array($result); $i++) {
                        // 상품이미지
                        $image = get_it_image($row['it_id'], 50, 50);

                        // 상품의 옵션정보
                        $sql = " select ct_id, it_id, ct_price, ct_point, ct_qty, ct_option, ct_status, cp_price, ct_stock_use, ct_point_use, ct_send_cost, io_type, io_price
                        from {$g5['g5_shop_cart_table']}
                        where od_id = '{$od['od_id']}'
                          and it_id = '{$row['it_id']}'
                        order by io_type asc, ct_id asc ";
                        $res = sql_query($sql);
                        $rowspan = sql_num_rows($res);

                        // 합계금액 계산
                        $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                            SUM(ct_qty) as qty
                        from {$g5['g5_shop_cart_table']}
                        where it_id = '{$row['it_id']}'
                          and od_id = '{$od['od_id']}' ";
                        $sum = sql_fetch($sql);

                        // 배송비
                        switch($row['ct_send_cost'])
                        {
                            case 1:
                                $ct_send_cost = '착불';
                                break;
                            case 2:
                                $ct_send_cost = '무료';
                                break;
                            default:
                                $ct_send_cost = '선불';
                                break;
                        }

                        // 조건부무료
                        if($row['it_sc_type'] == 2) {
                            $sendcost = get_item_sendcost($row['it_id'], $sum['price'], $sum['qty'], $od['od_id']);

                            if($sendcost == 0)
                                $ct_send_cost = '무료';
                        }

                        for($k=0; $opt=sql_fetch_array($res); $k++) {
                            if($opt['io_type'])
                                $opt_price = $opt['io_price'];
                            else
                                $opt_price = $opt['ct_price'] + $opt['io_price'];

                            // 소계
                            $ct_price['stotal'] = $opt_price * $opt['ct_qty'];
                            $ct_point['stotal'] = $opt['ct_point'] * $opt['ct_qty'];
                            ?>
                            <tr>
                                <?php if($k == 0) { ?>
                                    <td rowspan="<?php echo $rowspan; ?>">
                                        <a href="./itemform.php?w=u&amp;it_id=<?php echo $row['it_id']; ?>"><?php echo $image; ?> <?php echo stripslashes($row['it_name']); ?></a>
                                        <?php if($od['od_tax_flag'] && $row['ct_notax']) echo '[비과세상품]'; ?>
                                    </td>
                                <?php } ?>
                                <td>
                                    <?php echo get_text($opt['ct_option']); ?>
                                </td>
                                <td class="td_mngsmall"><?php echo $opt['ct_status']; ?></td>
                                <td class="td_num">
                                    <?php echo $chk_cnt; ?>
                                </td>
                                <td class="td_num"><?php echo number_format($opt_price); ?></td>
                                <td class="td_num"><?php echo number_format($ct_price['stotal']); ?></td>
                                <td class="td_num"><?php echo number_format($opt['cp_price']); ?></td>
                                <td class="td_num"><?php echo number_format($ct_point['stotal']); ?></td>
                                <td class="td_sendcost_by"><?php echo $ct_send_cost; ?></td>
                                <td class="td_mngsmall"><?php echo get_yn($opt['ct_point_use']); ?></td>
                                <td class="td_mngsmall"><?php echo get_yn($opt['ct_stock_use']); ?></td>
                            </tr>
                            <?php
                            $chk_cnt++;
                        }
                        ?>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>

        </table>

        <a href="<?php echo G5_ADMIN_URL.'/shop_admin/orderform.php?od_id='.$od_id; ?>" target="_blank" style="<?php echo $ft_a_st; ?>">관리자 모드에서 주문 확인</a>

    </div>
</div>

</body>
</html>
