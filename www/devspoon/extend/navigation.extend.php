<?php
/**************************
@Filename: navigation.extend.php
@Version : 0.1
@Author  : Freemaster
@Date  : 2016/04/12 화요일 오전 11:43:20
@Edit : 2018/07/18 수요일 오후 03:32:49
@Content : PHP by Editplus
**************************/
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

function get_top_navi($code="", $chasi=1)
{
    global $g5;

    $MenuSql = "";
    $MenuResult = "";
    $MenuRow = array();
    $chasiLength = $chasi*2;
    $nextChasi = $chasi+1;

    //메뉴 배열 저장
    $MenuWhere = " AND LENGTH(me_code) = '".$chasiLength."' ";
    if($chasi > 1)
        $MenuWhere .= " AND me_code LIKE '".$code."%' ";
    $MenuSql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND me_order > '0' ".$MenuWhere." ORDER BY me_order ";
    print_r($chasi." MenuSql =".$MenuSql);
    $MenuResult = sql_query($MenuSql);
    for($i=0; $MenuRow = sql_fetch_array($MenuResult); $i++) {
        $Menu[$i] = $MenuRow;
        $Menu[$i][$nextChasi] = get_top_navi($MenuRow['me_code'],$nextChasi);
        $Menu['cnt']++;
    }
    return $Menu;
}

function get_top_numnavi()
{
    global $g5;

    $bMenuSql = "";
    $bMenuResult = "";
    $bMenuRow = array();

    $b2MenuSql = "";
    $b2MenuResult = "";
    $b2MenuRow = array();

    $b3MenuSql = "";
    $b3MenuResult = "";
    $b3MenuRow = array();

    $b4MenuSql = "";
    $b4MenuResult = "";
    $b4MenuRow = array();

    //메뉴 배열 저장
    $bMenuSql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND me_order > '0' AND LENGTH(me_code) = '2' ORDER BY me_order";
    $bMenuResult = sql_query($bMenuSql,false);
    for($i=0; $bMenuRow = sql_fetch_array($bMenuResult); $i++) {
        $bMenu[$i] = $bMenuRow;
        //2차메뉴
        $b2MenuSql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND LENGTH(me_code) = '4' AND SUBSTRING(me_code, 1, 2) = '".$bMenuRow['me_code']."' ORDER BY me_order";
        $b2MenuResult = sql_query($b2MenuSql);
        for($j=0; $b2MenuRow = sql_fetch_array($b2MenuResult); $j++) {
            $bMenu[$i][$j] = $b2MenuRow;
            //3차메뉴
            $b3MenuSql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND LENGTH(me_code) = '6' AND SUBSTRING(me_code, 3, 4) = '".$b2MenuRow['me_code']."' ORDER BY me_order";
            $b3MenuResult = sql_query($b3MenuSql);
            for($k=0; $b3MenuRow = sql_fetch_array($b3MenuResult); $k++) {
                $bMenu[$i][$j][$k] = $b3MenuRow;
                //4차메뉴
                $b4MenuSql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND LENGTH(me_code) = '8' AND SUBSTRING(me_code, 3, 6) = '".$b3MenuRow['me_code']."' ORDER BY me_order";
                $b4MenuResult = sql_query($b4MenuSql);
                for($l=0; $b4MenuRow = sql_fetch_array($b4MenuResult); $l++) {
                    $bMenu[$i][$j][$k][$l] = $b4MenuRow;
                    $bMenu[$i][$j][$k]['cnt']++;
                }
                $bMenu[$i][$j]['cnt']++;
            }
            $bMenu[$i]['cnt']++;
        }
        $bMenu['cnt']++;
    }
    $bMenu['mns'] = $bMenu['cnt']?"mn".$bMenu['cnt']:"mn5";

    return $bMenu;
}

function get_middle_navi()
{
    global $g5, $group, $board, $co, $sca;
    global $gr_id, $bo_table, $co_id;

    //중단 메뉴 정의
    if($gr_id || $bo_table || $co_id) {
        if($gr_id) {
            $gRow = "";
            $pRow = array();
            $lMenuCnt = 0;
            $meLink = "gr_id=".$gr_id;
            if($bo_table) {
                $meLink = "bo_table=".$bo_table;
                /*
                if($sca) //뷴류를 메뉴에서 사용할 경우
                    $meLink .= "&sca=".$sca;
                */
            }
        }
        if($co_id) {
            $gRow = "";
            $pRow = array();
            $lMenuCnt = 0;
            $meLink = "co_id=".$co_id;
        }

        /* 1차메뉴 */
        $gSql = "SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND me_link LIKE ('%".$meLink."%') ";
        $gRow = sql_fetch($gSql);
        $gLen = strlen($gRow['me_code']);

        /* 링크로 메뉴를 찾았을 때 1차메뉴가 아니면 상위1차 메뉴의 내용을 코드로 호출 */
        if($gLen > 2) $gRow = get_menu_code($gRow['me_code'],1);

        /* 구해진 1차메뉴로 2차메뉴 구하기 */
        $Menu = get_menu_arr($gRow['me_code']);
    }
    return $Menu;
}

function get_menu_arr($code, $chasi=2)
{
    global $g5,$meLink;

    $chasiLength = $chasi*2;
    $nextChasi = $chasi+1;
    switch($chasi) { //차시에 따른 me_code 길이( 2자리는 계산하고 나머지는 그냥 불러오기 )
        case "1" : $chaLen = "2"; break;
        case "2" : $chaLen = "2"; break;
        case "3" : $chaLen = "4"; break;
        case "4" : $chaLen = "6"; break;
        case "5" : $chaLen = "8"; break;
        case "6" : $chaLen = "10"; break;
    }
    $codes = substr($code,0,$chaLen);

    $sql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND LENGTH(me_code) = '".$chasiLength."' AND SUBSTRING(me_code, 1, ".$chaLen.") = '".$codes."' ORDER BY me_order";
    $result = sql_query($sql);
    for($i=0; $row = sql_fetch_array($result); $i++) {
        $Menu[$i] = $row;
        $Menu[$i][$nextChasi] = get_menu_arr($row['me_code'],$nextChasi);
        $Menu['cnt']++;
    }
    return $Menu;
}

//get_menu_code 코드값, 원하는차시
function get_menu_code($code,$chasi="")
{
    global $g5;

    $chasiLength = $chasi*2;
    $nextChasi = $chasi+1;
    switch($chasi) { //차시에 따른 me_code 길이( 2자리는 계산하고 나머지는 그냥 불러오기 )
        case "1" : $chaLen = "2"; break;
        case "2" : $chaLen = "4"; break;
        case "3" : $chaLen = "6"; break;
        case "4" : $chaLen = "8"; break;
        case "5" : $chaLen = "10"; break;
        case "6" : $chaLen = "12"; break;
    }
    $codes = substr($code,0,$chaLen);

    $sql = " SELECT * FROM ".$g5['menu_table']." WHERE me_use = '1' AND LENGTH(me_code) = '".$chasiLength."' AND SUBSTRING(me_code, 1, ".$chaLen.") = '".$codes."' LIMIT 1";
    $row = sql_fetch($sql);
    return $row;
}

//현재페이지가 메뉴에서 어디인지 구하기(게시판)
function get_menu($id)
{
    global $g5;
    $query = "SELECT * FROM ".$g5['menu_table']." WHERE (1) AND LENGTH(me_code) > 2 AND me_link LIKE '%".$id."' ORDER BY me_code DESC";
    $row = sql_fetch($query);

    Return $row;
}

/* 현재페이지 정보 */
$co_id?$co_id:"";
$bo_table?$bo_table:'';
$thisCode = $co_id?$co_id:$bo_table;
$thisInfo = get_menu($thisCode);
$thisChasi = (strlen($thisInfo['me_code'])/2);
if($thisChasi != 1)
    $thisBInfo = get_menu_code($thisInfo['me_code'],1);
if($thisChasi != 2)
    $thisMInfo = get_menu_code($thisInfo['me_code'],2);

$mMenu['gTitle'] = $thisBInfo['me_name']?$thisBInfo['me_name']:$group['gr_subject'];
$mMenu['gLink'] = $thisBInfo['me_link']?$thisBInfo['me_link']:"";
//if($lMenu['cnt']) $lMenu['lmWidth'] = (int)100/$lMenu['cnt'];
$mMenu['mTitle'] = $thisMInfo['me_name']?$thisMInfo['me_name']:$thisInfo['me_name'];
if($sca) $mMenu['pTitle'] = urldecode($sca);


/* 예외 페이지 정보 */
if(empty($thisInfo)) {
    //회원가입페이지 등등의 페이지
    switch(basename($_SERVER['PHP_SELF'])) {
        case "login.php": $thisInfo['me_name'] = "로그인"; $lMenu['gTitle'] = "로그인"; $lMenu['pTitle'] = "로그인";
                break;
        case "register.php": $thisInfo['me_name'] = "회원가입"; $lMenu['gTitle'] = "회원가입"; $lMenu['pTitle'] = "회원가입";
                break;
        case "register_form.php": $thisInfo['me_name'] = "회원가입"; $lMenu['gTitle'] = "회원가입"; $lMenu['pTitle'] = "회원가입";
                break;
        case "register_result.php": $thisInfo['me_name'] = "회원가입"; $lMenu['gTitle'] = "회원가입"; $lMenu['pTitle'] = "회원가입";
                break;
        case "search.php": $thisInfo['me_name'] = "검색"; $lMenu['gTitle'] = "검색"; $lMenu['pTitle'] = "검색";
                break;
        case "faq.php": $thisInfo['me_name'] = "자주하는 질문"; $lMenu['gTitle'] = "자주하는 질문"; $lMenu['pTitle'] = "자주하는 질문";
                break;
        case "login_log.php": $thisInfo['me_name'] = "로그인기록"; $lMenu['gTitle'] = "로그인기록"; $lMenu['pTitle'] = "로그인기록";
                break;
        case "register_member.php": $thisInfo['me_name'] = "소셜로그인"; $lMenu['gTitle'] = "소셜로그인"; $lMenu['pTitle'] = "소셜로그인";
                break;
    }
}
?>
