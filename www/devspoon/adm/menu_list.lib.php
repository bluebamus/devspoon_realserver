<?php
/**************************
@Filename: menu_list.lib.php
@Version : 0.1
@Author  : Freemaster
@Date  : 2018/07/16 월요일 오후 01:45:20
@Content : PHP by Editplus
**************************/
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


function get_navi_menu($code="",$cha=1)
{
    global $g5;

    $cha2 = $cha*2;
    $where = "";
    $where = " AND LENGTH(me_code) = '".$cha2."' ";
    if($cha != 1)
        $where .= " AND me_code LIKE '".$code."%' ";

    $sql = " SELECT * FROM ".$g5['menu_table']." WHERE (1) ".$where." ORDER BY me_order ";

    $result = sql_query($sql);
    $cha3 = $cha+1;
    for($i=0; $row=sql_fetch_array($result); $i++) {
        $list[$i] = $row; //1차분류
        $list['cnt']++;
        $meCode = $list[$i]['me_code'];

        $bg = 'bg'.($i%2);
        $chaMenu = strlen($list[$i]['me_code'])/2; //메뉴단계
        $sub_menu_class = '';
        if(strlen($list[$i]['me_code']) > 2) { //if(strlen($list[$i]['me_code']) == 4) {
            $sub_menu_class = ' sub_menu_class'.$chaMenu;
            $sub_menu_info = '<span class="sound_only">'.$list[$i]['me_name'].'의 서브</span>';
            $sub_menu_ico = '<span class="sub_menu_ico"></span>';
        }

        $search  = array('"', "'");
        $replace = array('&#034;', '&#039;');
        $me_name = str_replace($search, $replace, $list[$i]['me_name']);
?>
    <tr class="<?php echo $bg; ?> menu_list menu_group_<?php echo substr($list[$i]['me_code'], 0, 2); ?> <?php echo strlen($list[$i]['me_code'])>2?'menu_group_'.$chaMenu.'_'.$list[$i]['me_code']:'menu_group_1_'.substr($list[$i]['me_code'],0,2);?>">
        <td class="td_category<?php echo $sub_menu_class; ?>">
            <input type="hidden" name="code[]" value="<?php echo substr($list[$i]['me_code'], 0, 2) ?>">
            <input type="hidden" name="code2[]" value="<?php echo $list[$i]['me_code'];?>">
            <input type="hidden" name="chaMenu[]" value="<?php echo $chaMenu;?>">
            <label for="me_name_<?php echo $i; ?>" class="sound_only"><?php echo $sub_menu_info; ?> 메뉴<strong class="sound_only"> 필수</strong></label>
            <input type="text" name="me_name[]" value="<?php echo $me_name; ?>" id="me_name_<?php echo $i; ?>" required class="required tbl_input2 full_input">
        </td>
        <td>
            <label for="me_link_<?php echo $i; ?>" class="sound_only">링크<strong class="sound_only"> 필수</strong></label>
            <input type="text" name="me_link[]" value="<?php echo $list[$i]['me_link'] ?>" id="me_link_<?php echo $i; ?>" required class="required tbl_input full_input">
        </td>
        <td class="td_mng">
            <label for="me_target_<?php echo $i; ?>" class="sound_only">새창</label>
            <select name="me_target[]" id="me_target_<?php echo $i; ?>">
                <option value="self"<?php echo get_selected($list[$i]['me_target'], 'self', true); ?>>사용안함</option>
                <option value="blank"<?php echo get_selected($list[$i]['me_target'], 'blank', true); ?>>사용함</option>
            </select>
        </td>
        <td class="td_num">
            <label for="me_order_<?php echo $i; ?>" class="sound_only">순서</label>
            <input type="text" name="me_order[]" value="<?php echo $list[$i]['me_order'] ?>" id="me_order_<?php echo $i; ?>" class="tbl_input" size="5">
        </td>
        <td class="td_mng">
            <label for="me_use_<?php echo $i; ?>" class="sound_only">PC사용</label>
            <select name="me_use[]" id="me_use_<?php echo $i; ?>">
                <option value="1"<?php echo get_selected($list[$i]['me_use'], '1', true); ?>>사용함</option>
                <option value="0"<?php echo get_selected($list[$i]['me_use'], '0', true); ?>>사용안함</option>
            </select>
        </td>
        <td class="td_mng">
            <label for="me_mobile_use_<?php echo $i; ?>" class="sound_only">모바일사용</label>
            <select name="me_mobile_use[]" id="me_mobile_use_<?php echo $i; ?>">
                <option value="1"<?php echo get_selected($list[$i]['me_mobile_use'], '1', true); ?>>사용함</option>
                <option value="0"<?php echo get_selected($list[$i]['me_mobile_use'], '0', true); ?>>사용안함</option>
            </select>
        </td>
        <td class="td_mng2">
            <button type="button" class="btn_add_submenu btn_03 "><?php echo ($chaMenu+1)."차메뉴";?>추가</button>
            <button type="button" class="btn_del_menu btn_02">삭제</button>
        </td>
    </tr>
<?php
        echo get_navi_menu($meCode,$cha3);
    }
}
?>
